#!/bin/bash
ENV=$1
ENVIRONMENTS='development|staging|production' # pipedelimited
PACKAGES='jq=apt install jq' # pipedelimited, element - equaldelimited

FONT_RED=`tput setaf 1`       # ${FONT_RED}
FONT_GREEN=`tput setaf 2`     # ${FONT_GREEN}
FONT_YELLOW=`tput setaf 3`    # ${FONT_YELLOW}
FONT_BLUE=`tput setaf 4`      # ${FONT_BLUE}
FONT_MAGENTA=`tput setaf 5`   # ${FONT_MAGENTA}
FONT_CYAN=`tput setaf 6`      # ${FONT_CYAN}
FONT_WHITE=`tput setaf 7`     # ${FONT_WHITE}
FONT_RESET=`tput sgr0`        # ${FONT_RESET}


checkPackages() {
    IFS='|' read -r -a ARRAY <<< $PACKAGES
	for PACKAGE in "${ARRAY[@]}"; do
        IFS='=' read -r -a ARRAY2 <<< $PACKAGE

        echo -n "Checking Pagckage (${ARRAY2[0]}): "

        if [ $(which ${ARRAY2[0]} | wc -m) -eq 0 ]; then
        	echo "${FONT_RED}${ARRAY2[0]} not installed.${FONT_RESET} Install using: ${FONT_YELLOW}${ARRAY2[1]}${FONT_RESET}"
            EXIT=1
        else
            echo "${FONT_GREEN}ok${FONT_RESET}"
        fi
	done

    if [ $EXIT ]; then
        exit 1
    fi

    echo ""
}

checkEnvironment() {
    echo -n "Checking Environment from script parameter (${ENV}): "

    if [ $(grep -w "${ENV}" <<< $ENVIRONMENTS | wc -c) -eq 0 ]; then
        echo "${FONT_RED}error.${FONT_RESET} Valid envs are: ${FONT_YELLOW}$(echo $ENVIRONMENTS | tr -s '|' ' ')${FONT_RESET}"
        exit 1
    else
        echo "${FONT_GREEN}ok${FONT_RESET}"
    fi

    echo ""
}

updateStartScript() {
    BACKUP=`jq .scripts.start package.json`

    echo -n "Updating Start Script Environment (${BACKUP}): "

    case $ENV in
        staging)    VAL="staging";;
        production) VAL="production";;
        *) VAL="dev";;
    esac

    REPLACEMENT="cross-env NODE_ENV=${VAL} FORCE_COLOR=true nodemon --inspect=5001 --config nodemon.js src/server.js"


    cat <<< $(jq ".scripts.start = \"${REPLACEMENT}\"" package.json) > package.json

    echo "${FONT_GREEN}ok${FONT_RESET}"
}


main() {
    # check required ubuntu package
    checkPackages

    # validate environment from script param and machine hostname
    checkEnvironment

    # check if the config files exists
    updateStartScript
}


main
