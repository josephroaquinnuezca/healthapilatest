// Multer is required to process file uploads and make them available via
// req.files
const Multer = require('multer');

const whitelistFile = [
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/pdf',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'text/csv',
    'application/vnd.ms-excel',
]
const whitelistImages = [
    'image/png',
    'image/jpeg',
    'image/jpg',
    'image/webp',
    'image/svg',
    'image/gif'
]

const fileFilter = (req, file, next) => {
    // console.log(req.files);
    // console.log(file);
    // console.log(JSON.stringify(next));
    // console.log(res);

    // return
    if(req.files.uploadedFiles){
        if (!whitelistFile.includes(file.mimetype) && file.fieldname == 'uploadedFiles') {
            console.log('file/minetype', file.mimetype)
            console.log('if A')
            next('File is not a valid csv.')
        }
        next(null, true)
    }
    if(req.files.uploadedImages){
        if (!whitelistImages.includes(file.mimetype) && file.fieldname == 'uploadedImages' ) {
            console.log('if B')
            next('File is not a valid image.')
        }
        next(null, true)
    }
};

const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 5 * 1024 * 1024, // no larger than 5mb, you can change as needed.
    },
    fileFilter: fileFilter
});

const fieldNames = [
    { name: 'uploadedFiles', maxCount: 1 },
    { name: 'uploadedImages' }
]

// Middleware function it is use as wrapper for multer middleware upload
function multerMiddleware(multerUpload) {
    return (req, res, next) =>
        multerUpload(req, res, err => {
            // handle Multer error
            if (err && err.name && err.name === 'MulterError') {
            // if (err instanceof multer.MulterError) {
                return res.status(500).send({
                    status: "500",
                    error: err.name,
                    message: `File upload error: ${err.message}`,
                    description: err,
                });
            }
            // handle other errors
            if (err) {
                return res.status(500).send({
                    status: "500",
                    error: 'FILE UPLOAD ERROR',
                    message: `Something wrong ocurred when trying to upload the file`,
                    description: err
                });
            }

            next();
        });
}

let upload = multerMiddleware(multer.fields(fieldNames))
module.exports = upload;

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, './uploads');
//     },
//     filename: function (req, file, cb) {
//         cb(null, Date.now() + "--" + file.originalname);
//     }
// });
