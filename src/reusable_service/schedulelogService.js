const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const ScheduleLogModel = db.ScheduleLog;

const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");

module.exports = {
  //CREATE USER FUNCTIONS
  ScheduleLog: async (data) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("TEAM USER INFO:"));
    //   schedule_id,
    // slog_id,
    // old_date,
    // old_start_time,
    // old_end_time,
    // old_duration_hrs,
    // old_duration_min,
    // old_visit_type,
    // new_date,
    // new_start_time,
    // new_end_time,
    // new_duration_hrs,
    // new_duration_min,
    // new_visit_type,
    // created_by,
    // created_date

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var schedule_log = {
        schedule_id : data.schedule_id,
        slog_id : data.slog_id,
        old_date : data.old_date,
        old_start_time : data.old_start_time,
        old_end_time : data.old_end_time,
        old_duration_hrs : data.old_duration_hrs,
        old_duration_min : data.old_duration_min,
        old_visit_type : data.old_visit_type,
        new_date : data.new_date,
        new_start_time : data.new_start_time,
        new_end_time : data.new_end_time,
        new_duration_hrs : data.new_duration_hrs,
        new_duration_min : data.new_duration_min,
        new_visit_type : data.new_visit_type,
        created_by : data.created_by
      };

      //console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));

      ScheduleLogModel.create(schedule_log)
        .then(async(data) => {
          //console.log(chalk.green('data:', JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          resolve(data);
        })
        .catch(async(err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  getScheduleLog: async (schedule_id) => {
    return new Promise(async(resolve, reject) => {
      
      await db.sequelize.query("Select A.created_date as log_date, A.*, B.* from schedule_log A left join schedule_log_master B on B.slog_id = A.slog_id where A.schedule_id = :SCHEDID order by A.created_date DESC", { 
            
       logging: console.log,
       // Set this to true if you don't have a model definition for your query.
       replacements: {SCHEDID: schedule_id}, 
       raw: false,
       type: QueryTypes.SELECT })
        .then(async(data) => {
          // console.log(chalk.green('data:', JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },
}
