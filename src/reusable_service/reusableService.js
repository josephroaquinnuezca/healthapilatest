
const { QueryTypes } = require('sequelize');
// declaring models
const db = require("../models");

//initializing models
const ReusableModel = db.Reusable;

const Op = db.Sequelize.Op;
const chalk = require('chalk');
const async = require('async');




module.exports = {

      //CREATE USER FUNCTIONS
      SystemLog: async (user_id, username, route, message, result, req_type) => {
        return new Promise((resolve, reject) => {
            
          // console.log(chalk.yellow("TEAM USER INFO:"));

          //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
          var data_message = {
    
            user_id: user_id,
            username: username,
            route: route,
            message: message,
            result: result,
            req_type: req_type,
          
          };
    
          //console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
          
          ReusableModel.create(data_message)
          .then(data => {
           //console.log(chalk.green('data:', JSON.stringify(data)));
            // console.log(chalk.green('data:', data));
            resolve(data);
          })
          .catch(err => {
            console.log(chalk.red('err: ey' , err))
            reject(err);
          });
        
        });
      },

    

}