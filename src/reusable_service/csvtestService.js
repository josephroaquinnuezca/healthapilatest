const { QueryTypes } = require('sequelize');
// declaring models
const db = require("../models");
const sequelize = require("sequelize");

//initializing models
const CsvModel = db.CsvModel;
const Progress = db.Progress;
const TemporaryCsvModel = db.TemporaryCsvModel;
const TerritoryStoreAllocation = db.TerritoryStoreAllocation;

const Op = db.Sequelize.Op;
const chalk = require('chalk');
const async = require('async');

const ReusableService = require('../../src/reusable_service/reusableService');

const { ValidationError } = db.Sequelize;


// const csv = require('fast-csv');
//for csv file upload
const fs = require('fs');
const path = require('path');
const csv = require('csvtojson')

var iconv = require('iconv-lite');
const streamifier = require('streamifier');

const Encoding = require('encoding-japanese');

//LOCAL FUNCTIONS
//data, password
const funcCondition = (data, loginUserID) => {
  if (data) {
  
    return { covering_activeness: '1', created_by: Number(loginUserID), updated_date: null}
  } else {
  
      return { covering_activeness: '1', updated_by: Number(loginUserID)}
  }
}
  


module.exports = {


  CreatecsvService: async (fileBuffer, loginUserID) => {
            return new Promise(async (resolve, reject) => {
              var myObj = []
              var counter = 0;
              var progressID;
              // uncomment can be use to validate file
              var codes = new Uint8Array(fileBuffer);
              var charsetMatch = Encoding.detect(codes);
              console.log(codes);
              console.log("DETECT TYPE ====>",charsetMatch);

              //transacation here
              // First, we start a transaction and save it into a variable
              const t1 = await db.sequelize.transaction();
            

              // First, we start a transaction and save it into a variable
              // const t =  await db.sequelize.transaction();

              let buffer = fileBuffer;
              
              const stream = streamifier.createReadStream(buffer).pipe(iconv.decodeStream(charsetMatch)); // encoding into SJIS
              
              await csv({
                headers: [
                  'unique_id',
                  'ice_id',
                  'store_type',
                  'product',
                  'partner',
                  'channel',
                  'tier',
                  'branch',
                  'area',
                  'prefecture',
                  'covered_store_name',
                  'territory_name',
                  'frequency',
                  'visit_per_month',
                  'first_agent',
                  'second_agent',
                  'fixture_flag',
                  'pixel_fixture',
                  'zip',
                  'address',
                  'tel',
                  'branch_num',
                  'area_num',
                  'prefecture_num',
                  'in_tsm',
                  'addtional_flag',
                  'sb_flag',
                  'kddi_flag',
                  'changed_flag',
                  'changed_contents',
                  'covering_activeness',
                  'created_by',
                  'created_date',
                  'updated_by',
                  'updated_date'],
                ignoreEmpty: true,
                nullObject: null,
              })
              /**
              * @param {object} json - return the obj value
              * @param {number} lineNumber - return number index
              * note: you can also use promise inside it will take a while but you can make more algos inside.
              */
              .fromStream(stream).subscribe(async (json, lineNumber) => {
                // return new Promise((resolveStream, rejectStream) => {
                  myObj.push(json);
                  counter++;
                  // console.log(counter)
                // }) // end promise
              }) // end subscribe
              console.log("TAPOS NA ANG myObj at counter.");
        
              // LAGAY NYO SA SEPARATE FUNCTION IF GUSTO NYO PERO DATA ANG RESOLVE TRUE LANG
              // DITO KAYO GAGAWA NG PROGRESS ID
              let createProgress = await Progress.create({})
              .then(data=>{
                console.log(data);
                resolve(data);
                progressID = data.dataValues.id
                return true;
              })
              .catch(async err=>{
                try{
                  let updateProgressStatus = await module.exports.updateProgressStatus("CreatecsvService: " + err, "failed", progressID)
                  console.log(err);
                  reject(err);
                  return false;

                }catch(err){
                  reject(err);
                }
                
              })
              
              //dito yata ang setflagtonull
               let setAllFlagToNull = await module.exports.setAllFlagToNull(t1, loginUserID, progressID);
              // console.log("return by createProgress: ", createProgress)
               //return;
        
              if (!createProgress) return;
        
              console.log("PERO NAG RUN PADIN DITO")

              
        
        
              // resolve(setAllFlagToNull)
              // return
              async.forEachSeries(Object.keys(myObj), async (keyValue, cb) =>{
                // const salt = await bcrypt.genSalt(10);
                // const pw = await bcrypt.hash(myObj[keyValue].password, salt);

                console.log("KEY VALUE", keyValue)
                //INDEX NIYA YUNG KEY VALUE
                try {

                  //ITO YUNG UNA NA SAVING
                  let upsertData = await module.exports.upsertData(myObj[keyValue], keyValue, t1, myObj.length, progressID);

                  // console.log("bweset", upsertData)

                  // console.log("NATAPOS ANG ISANG PROMISE")
                  let updateUpserts = await module.exports.updateUpserts(upsertData, keyValue, loginUserID, myObj.length, t1, progressID);
        
                  //let resetProgress = await module.exports.resetProgress(progressID);

                  // console.log("NATAPOS ANG PANGALAWANG PROMISE")
                } catch (error) {
                  try {
                    let updateProgressStatus = await module.exports.updateProgressStatus("forEachSeries: " + error, "failed", progressID)
                    await t1.rollback()
                    // await t2.rollback()
                    reject(error);
                    
                  } catch (error) {
                    reject(error)
                  }
                }
        
                }, async completed => {
                  try {
                    //let updateProgressStatus = await module.exports.updateProgressStatus("Success 100%", "success", progressID)
           
                    // console.log("HINDI NAG ANTAY");
                    console.log(myObj.length);
                    console.log(counter);
                    // console.log(completed);
                    await t1.commit();
                    // await t2.commit();
                    resolve();
                    
                  } catch (error) {
                    reject(error);
                  }
                 
              });
            })
          },
          
          //ALL
          setAllFlagToNull: async (t, loginUserID, progressID) => {
            return new Promise((resolve, reject) => {

              db.sequelize.query(`update covered_store_master set covering_activeness = '0', updated_by = '${loginUserID}'`, { 
                transaction: t,
               raw: false,
              })
                .then(data => {
                  resolve();
                })
                .catch(async err => {
                  try {
                    let updateProgressStatus = await module.exports.updateProgressStatus("setAllFlagToNull: " + err, "failed", progressID)
                    console.log(chalk.red('err:' , err))
                    reject(err);
                    
                  } catch (error) {
                    reject(error);
                  }
                });

            })
          },

          upsertData: async (myObj, keyValue, t, myObjLength, progressID) => {
            return new Promise(async (resolve, reject) => {
              // NOTE DO THE HASING ON THE BACKGROUND NEXT TASK
              // const salt = await bcrypt.genSalt(10);
              // const pw = await bcrypt.hash(myObj.password, salt);
              // myObj.password = pw;
              
              // /**
              // * @param {options} transaction - create transaction for commit and rollback
              // */

              console.log("My object 2nd function", myObj)

              CsvModel.upsert(myObj,{
                transaction: t
              })
                /**
                * Success upsert.
                * @param {array} data - contains upsert user data & boolean if update or insert
                * @param {object} data[0] - Contains Returning Result of Successful update or insert
                * @param {boolean} data[1] - boolean if update or insert #true=Insert #false=Updated
                */
                .then(async data => {
                  // console.log('upserting: ', (Number(keyValue)+1), '-', myObjLength);
                  resolve(data);
                })
                .catch(async err => {
                  try {
                    let updateProgressStatus = await module.exports.updateProgressStatus("upsertData: " + err, "failed", progressID)
                    console.log("Run DITO");
                    if (err instanceof ValidationError) {
                      console.log("Run A");
                      console.log(err.errors[0]);
                      console.log(chalk.red('err instanceof ValidationError:', err.errors[0].message));
                      //modify reject('Captured validation error: ' + err.errors[0].message + " on username: " + err.errors[0].instance.username + " on email: " + err.errors[0].instance.email_address + " Line: " + (keyValue + 2));
                    } else {
                      console.log("Run B");
                      console.log(chalk.red('err:', err))
                      reject(err);
                    }
                    
                  } catch (error) {
                    reject(error);
                  }
                }) // end user db operation
            })
          },

          //new
          // LETTER C
          updateUpserts: async (data, keyValue, loginUserID, myObjLength, t, progressID) => {
            return new Promise((resolve, reject) => {
              ////////////////////////////////////////////////////////////////////////////
              // THIS IF FOR UPDATING IF INSERTED OR UPDATED
              // ADD CREATED BY IF INSERT | ADD UPDATED BY IF UPDATE

              let value = funcCondition(data[1], loginUserID) //add Password and LogInUser
              //condition, 
              console.log("DITO YUNG MALI updateUpserts", value)

              CsvModel.update(value,{
                where: { unique_id: data[0].unique_id },
                transaction: t
              })
              .then(async updated_data => {
                // console.log('created by & updated by - modify: ', (Number(keyValue)+1), '-', myObjLength);
                let updateProgress = await module.exports.updateProgress(keyValue, myObjLength, progressID)
                resolve()
              })
              .catch(async err => {
                try {
                  let updateProgressStatus = await module.exports.updateProgressStatus("updateUpserts: " + err, "failed", progressID)
                  console.log("DITO YUNG MALI updateUpserts", value)
                  reject('something went wrong while updating and fetching your data.' + err);
                  
                } catch (error) {
                  reject(error)
                }
                
              });
              // END PUT CREATED BY OR UPDATE BY ON INSERTED OR UPDATED
              // -------------------------------------------------------------------------
            })
          },

          // new
          updateProgress: async(keyValue, myObjLength, progressID) => {
            return new Promise((resolve, reject) => {
              ////////////////////////////////////////////////////////////////////////

             
              // FOR PROGRESS BAR
              Progress.update({ progress: ((Number(keyValue)+1) / myObjLength) * 100 }, {
                where: { id: progressID }
              }).then(async res => {
                if(res.progress == 100){
                  let updateProgressStatus = await module.exports.updateProgressStatus("Success 100%", "success", progressID)
                }
                // console.log('updating the progress: ', ((Number(keyValue)+1) / myObjLength) * 100);

                resolve();
              }).catch(async err => {
                try {
                  let updateProgressStatus = await module.exports.updateProgressStatus("updateProgress: " + err, "failed", progressID)
                  console.log("DITO YUNG MALI upadteProgress")
                  reject('something went wrong while updating and fetching your data.' + err);
                  
                } catch (error) {
                  reject(error)
                }
              });
                // END PROGRESS BAR
                // -------------------------------------------------------------------
            })
          },
          updateProgressStatus: async(err, status, progressID) => {
            return new Promise((resolve, reject) => {
              ////////////////////////////////////////////////////////////////////////
              // FOR PROGRESS BAR
              Progress.update({ 
                description: err,
                status: status
              }, {
                where: { id: progressID }
              }).then(res => {
                // console.log('updating the progress: ', ((Number(keyValue)+1) / myObjLength) * 100);
                resolve();
              }).catch(err => {
                reject('something went wrong while updating progress table.' + err);
              });
                // END PROGRESS BAR
                // -------------------------------------------------------------------
            })
          },
          
          //new
          resetProgress: async(progressID) => {
            return new Promise((resolve, reject) => {
              ////////////////////////////////////////////////////////////////////////
              // FOR PROGRESS BAR
              Progress.update({ progress: 0 }, {
                where: { id: progressID }
              }).then(res => {
                // console.log('updating the progress: ', ((Number(keyValue)+1) / myObjLength) * 100);
                resolve();
              }).catch(err => {
                reject('something went wrong while updating and fetching your data.' + err);
              });
                // END PROGRESS BAR
                // -------------------------------------------------------------------
            })
          },
          // new

          getProgress: async(id, logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await Progress.findByPk(id )
              .then(async res => {
                console.log(res)
                
                resolve(res)
              }).catch(err => {
                const error =  "'" + err + "'";
                ReusableService.SystemLog(logInId, mail_address, '/progress/:id', error, 'FETCH', 'GET');
                reject(err)
              })
            })
          },

          getAllDataCSV: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              
              
              await db.sequelize.query("SELECT A.covered_store_id, B.territory_id, A.created_by, A.created_date, A.updated_by, A.updated_date FROM covered_store_master A LEFT JOIN territory_master B ON A.territory_name = B.territory_name;", { 
                
                raw: false,
                type: QueryTypes.SELECT } )
                .then(async(data) => {
                  // console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch((err) => {
                  console.log(chalk.red("err:", err));
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-all-data-csv', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },

          // For the analyzing before submit

          truncateTbl: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await TemporaryCsvModel.destroy({ truncate : true, cascade: false } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/truncate-tbl', error, 'TRUNCATE', 'DESTROY');
                  reject(err);
                });
            });
          },

          CreateTemporaryCsv: async (fileBuffer, logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              // var data = [];

              // const stream = streamifier.createReadStream(CSVData).pipe(iconv.decodeStream("Shift_JIS"));
              // await data()
              // .fromStream(stream).subscribe(async (json, lineNumber) => {
              //   // return new Promise((resolveStream, rejectStream) => {
              //     data.push(json);
              //     counter++;
              //     // console.log(counter)
              //   // }) // end promise
              // }) // end subscribe
              // console.log("TAPOS NA ANG myObj at counter.");
              var myObj = []
              var counter = 0;
              var progressID;
              // uncomment can be use to validate file
              var codes = new Uint8Array(fileBuffer);
              var charsetMatch = Encoding.detect(codes);
              console.log(codes);
              console.log("DETECT TYPE ====>",charsetMatch);

            

              // First, we start a transaction and save it into a variable
              // const t =  await db.sequelize.transaction();

              let buffer = fileBuffer;
              
              const stream = streamifier.createReadStream(buffer).pipe(iconv.decodeStream(charsetMatch)); // encoding into SJIS
              
              await csv({
                headers: [
                  'unique_id',
                  'ice_id',
                  'store_type',
                  'product',
                  'partner',
                  'channel',
                  'tier',
                  'branch',
                  'area',
                  'prefecture',
                  'covered_store_name',
                  'territory_name',
                  'frequency',
                  'visit_per_month',
                  'first_agent',
                  'second_agent',
                  'fixture_flag',
                  'pixel_fixture',
                  'zip',
                  'address',
                  'tel',
                  'branch_num',
                  'area_num',
                  'prefecture_num',
                  'in_tsm',
                  'addtional_flag',
                  'sb_flag',
                  'kddi_flag',
                  'changed_flag',
                  'changed_contents',
                  'covering_activeness',
                  'created_by',
                  'created_date',
                  'updated_by',
                  'updated_date'],
                ignoreEmpty: true,
                nullObject: null,
              })
              /**
              * @param {object} json - return the obj value
              * @param {number} lineNumber - return number index
              * note: you can also use promise inside it will take a while but you can make more algos inside.
              */
              .fromStream(stream).subscribe(async (json, lineNumber) => {
                // return new Promise((resolveStream, rejectStream) => {
                  myObj.push(json);
                  counter++;
                  // console.log(counter)
                // }) // end promise
              }) // end subscribe
              
              await TemporaryCsvModel.bulkCreate(myObj)
                .then(function() {

                  //(if you try to immediately return the Model after bulkCreate, the ids may all show up as 'null')
                   return TemporaryCsvModel.findAll()
                 } )
              .then(async(data) => {
               console.log(chalk.green('data:', JSON.stringify(data)));
                // console.log(chalk.green('data:', data));
                
                resolve(data);
              })
              .catch(err => {
                console.log(chalk.red('err: ey' , err))
                const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/post-temporary-csv', error, 'TRUNCATE', 'DESTROY');
                reject(err);
              });
            
            });
          },

          getNotTerritory: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {
              await db.sequelize.query("Select A.covered_store_id, A.unique_id, A.territory_name, A.frequency, A.covered_store_name from temporary_csv A where A.territory_name != 'N/A' and A.territory_name not in (select B.territory_name from territory_master B where B.status = 9);", { 
                
               raw: false,
               type: QueryTypes.SELECT } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-not-territory', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },
          getNotFrequency: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {
              await db.sequelize.query("Select A.covered_store_id, A.unique_id, A.territory_name, A.frequency, A.covered_store_name from temporary_csv A where A.frequency != 'N/A' and  A.frequency not in (select C.visit_frequency_name from visit_frequency_master C);", { 
                
               raw: false,
               type: QueryTypes.SELECT } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-not-frequency', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },

          getA: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await db.sequelize.query("Select count(A.unique_id)as con_a from temporary_csv A where A.unique_id not in (select B.unique_id from covered_store_master B);", { 
                
               raw: false,
               type: QueryTypes.SELECT } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-a', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },
          getC: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await db.sequelize.query("Select count(A.unique_id)as con_c from temporary_csv A where A.unique_id in (select B.unique_id from covered_store_master B);", { 
                
               raw: false,
               type: QueryTypes.SELECT } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-c', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },
          getB: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await db.sequelize.query("Select count(A.unique_id)as con_b from covered_store_master A where A.unique_id not in (select B.unique_id from temporary_csv B);", { 
                
               raw: false,
               type: QueryTypes.SELECT } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/get-b', error, 'FETCH', 'GET');
                  reject(err);
                });
            });
          },

          //Saving to the territory store allocation
          truncateTblTSA: async (logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              

              await TerritoryStoreAllocation.destroy({ truncate : true, cascade: false } )
      
                .then(async(data) => {
                  console.log(chalk.green('data:', JSON.stringify(data)));
                  // console.log(chalk.green('data:', data));
                  
                  resolve(data);
                })
                .catch(err => {
                  console.log(chalk.red('err:' , err))
                  const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/truncate-tbl-tsa', error, 'TRUNCATE', 'DESTROY');
                  reject(err);
                });
            });
          },
          CreateTerritoryStoreAllocation: async (CoveredStoreData, logInId, mail_address) => {
            return new Promise(async(resolve, reject) => {

              
                
              await TerritoryStoreAllocation.bulkCreate(CoveredStoreData)
                .then(function() {

                  //(if you try to immediately return the Model after bulkCreate, the ids may all show up as 'null')
                   return TerritoryStoreAllocation.findAll()
                 } )
              .then(async(data) => {
               console.log(chalk.green('data:', JSON.stringify(data)));
                // console.log(chalk.green('data:', data));
                
                resolve(data);
              })
              .catch(err => {
                console.log(chalk.red('err: ey' , err))
                const error =  "'" + err + "'";
                    ReusableService.SystemLog(logInId, mail_address, '/post-territory-store-allocation', error, 'CREATE', 'POST');
                reject(err);
              });
            
            });
          },
}
          

         
          

