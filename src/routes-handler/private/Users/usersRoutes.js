const router = require('express').Router();
const UserInfoController = require('./usersController');
const RouteConstant = require('../../../constant/Routes');
const Middleware = require("../../../cors/middleware").checkToken;

module.exports = (app) => {

    router.route('/user-get-single-unique-code')
    .get(UserInfoController.getSingleUniqueCode);


    router.route('/user-post-data')
    .post(UserInfoController.createUserInfo);

    router.route('/user-get-all-data')
    .get(UserInfoController.getAllUserInfo);

    
    router.route('/user-get-single-data')
    .get(UserInfoController.getSingleUserInfo);

    

    router.route('/user-update-single-data')
    .patch(UserInfoController.updateSingleUserInfo);

    router.route('/user-delete-single-data')
    .get(UserInfoController.deleteSingleUserInfo);
    
    //patay
    router.route('/user-archived-deceased')
    .patch(UserInfoController.archivedDeceased);

    router.route('/user-archived-child-status')
    .patch(UserInfoController.archivedchildstatus);

    router.route('/user-archived-child-status-history')
    .get(UserInfoController.getAllChildHistoy);

    //women

    router.route('/user-archived-women-period-history')
    .get(UserInfoController.getAllWomensPeriod);
    

    router.route('/user-get-all-child-count')
    .get(UserInfoController.getAllChildCount);

    //population
    router.route('/user-get-all-population-count')
    .get(UserInfoController.getAllPopulationCount);

    //MalepopulationZone
    router.route('/user-get-all-male-zone')
    .get(UserInfoController.getAllMaleZone);

     //FeMalepopulationZone
     router.route('/user-get-all-female-zone')
     .get(UserInfoController.getAllFemaleZone);

     //pregnant
     router.route('/user-get-all-pregnant')
     .get(UserInfoController.getAllPregnant);

      //senior
      router.route('/user-get-all-senior')
      .get(UserInfoController.getAllsenior);

    //report
    router.route('/user-get-all-report')
    .get(UserInfoController.getAllUsersReport)

    //add schedule on pregnant

    
    router.route('/add-schedule-pregnant')
    .post(UserInfoController.addPregnantSchedule);

    app.use(
        RouteConstant.USER_PRIVATE,
        // Middleware,
        router
    );
};
