const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

const bcrypt = require("bcrypt");

//LOCAL
const UserInfoService = require("../../.././services/UsersService");
// const UtilitiesService = require('../../.././services/UtilitiesService');
const reqResponse = require("../../../cors/responseHandler");
let jwt = require("jsonwebtoken");

const chalk = require("chalk");
const async = require("async");
// const ReusableService = require("../../../reusable_service/reusableService");

module.exports = {

  getSingleUniqueCode: async (req, res) => {
    try {

      const unique_code = req.query.unique_code;

      // return;
      let result = await UserInfoService.getSingleUniqueCode(unique_code);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Single Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  createUserInfo: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        fname: req_body.fname,
        mname: req_body.mname,
        lname: req_body.lname,
        suffix: req_body.suffix,
        bdate: req_body.bdate,
        age: req_body.age,
        sex: req_body.sex,
        house_no: req_body.house_no,
        zone: req_body.zone,
        province: req_body.province,
        city: req_body.city,
        barangay: req_body.barangay,
        cell_no: req_body.cell_no,
        pwd: req_body.pwd,
        systolic: req_body.systolic,
        diastolic: req_body.diastolic,
        medical_history: req_body.medical_history,
        child_height: req_body.child_height,
        child_weight: req_body.child_weight,
        child_body_mass_index: req_body.child_body_mass_index,
        child_status: req_body.child_status,
        type: req_body.type,
        maintenance_senior: req_body.maintenance_senior,
        maintenance_pregnant: req_body.maintenance_pregnant,
        pregnant_status: req_body.pregnant_status,
        type_maintenance: req_body.type_maintenance,
        last_period: req_body.last_period,
        estimated_period: req_body.last_period,
        person_status: req_body.person_status,
      };

      // return;
      let result = await UserInfoService.createUserInfo(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllUserInfo: async (req, res) => {
    try {
      // let id = req.params.id;
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await UserInfoService.getAllUserInfo(
        limit,
        offset,
        searchString
      );

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getSingleUserInfo: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await UserInfoService.getSingleUserInfo(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Single Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  updateSingleUserInfo: async (req, res) => {
    try {
      const id = parseInt(req.query.id);
      const req_body = req.body;

      const data = {
        fname: req_body.fname,
        mname: req_body.mname,
        lname: req_body.lname,
        suffix: req_body.suffix,
        bdate: req_body.bdate,
        age: req_body.age,
        sex: req_body.sex,
        house_no: req_body.house_no,
        zone: req_body.zone,
        province: req_body.province,
        city: req_body.city,
        barangay: req_body.barangay,
        cell_no: req_body.cell_no,
        // child_height: req_body.child_height,
        // child_weight: req_body.child_weight,
        // child_body_mass_index: req_body.child_body_mass_index,
        // type: req_body.type,
        // maintenance_senior: req_body.maintenance_senior,
        // maintenance_pregnant: req_body.maintenance_pregnant,
        // pregnant_status: req_body.pregnant_status,
        // pregnant_status: req_body.pregnant_status,
      };

      // return;
      let result = await UserInfoService.updateSingleUserInfo(data, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  deleteSingleUserInfo: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      console.log("Error: delete user", id)

      // return;
      let result = await UserInfoService.deleteSingleUserInfo(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Deleted.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  archivedchildstatus: async (req, res) => {
    try {


      const id = parseInt(req.query.id);

      const req_body = req.body;

      const insert_data = {
        user_id: id,
        fname: req_body.fname,
        mname: req_body.mname,
        lname: req_body.lname,
        suffix: req_body.suffix,
        bdate: req_body.bdate,
        age: req_body.age,
        sex: req_body.sex,
        house_no: req_body.house_no,
        zone: req_body.zone,
        province: req_body.province,
        city: req_body.city,
        barangay: req_body.barangay,
        cell_no: req_body.cell_no,
        pwd: req_body.pwd,
        child_height: req_body.child_height,
        child_weight: req_body.child_weight,
        child_body_mass_index: req_body.child_body_mass_index,
      };

      const update_data = {
        bdate_update: req_body.bdate_update,
        age_update: req_body.age_update,
        child_height_update: req_body.child_height_update,
        child_weight_update: req_body.child_weight_update,
        child_body_mass_index_update: req_body.child_body_mass_index_update,
      };

      console.log(update_data)

      // return;
      let result = await UserInfoService.archivedchildstatus(insert_data, update_data, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllChildHistoy: async (req, res) => {
    try {
      // let id = req.params.id;

      
      let id = parseInt(req.query.id);
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await UserInfoService.getAllChildHistoy(
        limit,
        offset,
        searchString,
        id,
      );

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllWomensPeriod: async (req, res) => {
    try {
      // let id = req.params.id;

      
      let id = parseInt(req.query.id);
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await UserInfoService.getAllWomensPeriod(
        limit,
        offset,
        searchString,
        id,
      );

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  //COUNT CHILD STATUS
  getAllChildCount: async (req, res) => {
    try {

      let result = await UserInfoService.getAllChildCount();

     
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  //COUNT Population
  getAllPopulationCount: async (req, res) => {
    try {

      let result = await UserInfoService.getAllPopulationCount();

     
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

    //COUNT Male
    getAllMaleZone: async (req, res) => {
      try {
  
        let result = await UserInfoService.getAllMaleZone();
  
       
        res
          .status(201)
          .send(
            reqResponse.successResponse(
              201,
              "Successfully Get All Data.",
              result
            )
          );
      } catch (error) {
        console.error("catchFromController: ", error);
        res.status(502).send(reqResponse.errorResponse(502, String(error)));
      }
    }, //end

      //COUNT FeMale
      getAllFemaleZone: async (req, res) => {
        try {
    
          let result = await UserInfoService.getAllFemaleZone();
    
         
          res
            .status(201)
            .send(
              reqResponse.successResponse(
                201,
                "Successfully Get All Data.",
                result
              )
            );
        } catch (error) {
          console.error("catchFromController: ", error);
          res.status(502).send(reqResponse.errorResponse(502, String(error)));
        }
      }, //end

      //pregnent
        getAllPregnant: async (req, res) => {
          try {
      
            let result = await UserInfoService.getAllPregnant();
      
           
            res
              .status(201)
              .send(
                reqResponse.successResponse(
                  201,
                  "Successfully Get All Data.",
                  result
                )
              );
          } catch (error) {
            console.error("catchFromController: ", error);
            res.status(502).send(reqResponse.errorResponse(502, String(error)));
          }
        }, //end

        //senior
        getAllsenior: async (req, res) => {
          try {
      
            let result = await UserInfoService.getAllsenior();
      
           
            res
              .status(201)
              .send(
                reqResponse.successResponse(
                  201,
                  "Successfully Get All Data.",
                  result
                )
              );
          } catch (error) {
            console.error("catchFromController: ", error);
            res.status(502).send(reqResponse.errorResponse(502, String(error)));
          }
        }, //end
      
    
    
  





  getAllUsersReport: async (req, res) => {
    try {
    
      
      let days = req.query.days;
      let months = req.query.months;
      let years = req.query.years;
      let zones = req.query.zones;


      let person_status = req.query.person_status;
      let childStatusSearchString = req.query.childStatusSearchString;

      console.log("child status", childStatusSearchString )
      console.log("person status", person_status )

      console.log(days)
      console.log(months)
      console.log(years)

      // let id = parseInt(req.query.id);
      // let page = parseInt(req.query.page);
      // let limit = parseInt(req.query.size);

      let result = await UserInfoService.getAllUsersReport(person_status, childStatusSearchString);


      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  getSingleStatusMalnourished: async (req, res) => {
    try {

      let result = await UserInfoService.getSingleStatusMalnourished();

     
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end



  addPregnantSchedule: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        user_id: req_body.user_id,
        womens_date: req_body.womens_date,
        cell_no: req_body.cell_no,
        comment: req_body.comment
      };

      // return;
      let result = await UserInfoService.addPregnantSchedule(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end



//archive deceased


archivedDeceased: async (req, res) => {
  try {
    const id = parseInt(req.query.id);
    const req_body = req.body;

    const data = {
      fname: req_body.fname,
      mname: req_body.mname,
      lname: req_body.lname,
      suffix: req_body.suffix,
      bdate: req_body.bdate,
      age: req_body.age,
      sex: req_body.sex
    };
    // return;
    let result = await UserInfoService.archivedDeceased(data, id);

    res
      .status(201)
      .send(
        reqResponse.successResponse(201, "Successfully Deleted.", result)
      );
  } catch (error) {
    console.error("catchFromController: ", error);
    res.status(502).send(reqResponse.errorResponse(502, String(error)));
  }
}, //end

















};
