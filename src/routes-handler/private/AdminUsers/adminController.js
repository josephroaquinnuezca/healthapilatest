const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

const bcrypt = require("bcrypt");

//LOCAL
const AdminUser = require("../../.././services/TeamUserSettingService");
// const UtilitiesService = require('../../.././services/UtilitiesService');
const reqResponse = require("../../../cors/responseHandler");
let jwt = require("jsonwebtoken");

const chalk = require("chalk");
const async = require("async");
// const ReusableService = require("../../../reusable_service/reusableService");

module.exports = {
  getAllAdminUser: async (req, res) => {
    try {
      // let id = req.params.id;

      let id = parseInt(req.query.id);
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await AdminUser.getAllAdminUser(limit, offset, searchString);

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getSingleUsersdata: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await AdminUser.getSingleUsersdata(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Single Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  deleteSingleUsersdata: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await AdminUser.deleteSingleUsersdata(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Deleted.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  CreateAdminUser: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        name: req_body.name,
        mail_address: req_body.mail_address,
        password: req_body.password,
        user_role: req_body.user_role,
      };

      // return;
      let result = await AdminUser.CreateAdminUser(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  

  UpdateAdminUser: async (req, res) => {
    try {
      const id = parseInt(req.query.id);
      const req_body = req.body;

      const data = {
        name: req_body.name,
        mail_address: req_body.mail_address,
        password: req_body.password,
        user_role: req_body.user_role,
      };

      // return;
      let result = await AdminUser.UpdateAdminUser(data, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  SaveMedicationInfo: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        med_id: req_body.med_id,
        patient_id: req_body.patient_id,
        quantity:  req_body.quantity,
        med_available:  req_body.med_available,
        med_type:  req_body.med_type,
        med_dosage:  req_body.med_dosage,
        med_manufacturing: req_body.med_manufacturing,
        med_exdate:  req_body.med_exdate,
      };

      // return;
      let result = await AdminUser.SaveMedicationInfo(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  sendMalnourished: async (req, res) => {
    try {
     
      // return;
      let result = await AdminUser.sendMalnourished(req.body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  sendSenior: async (req, res) => {
    try {
     

      // return;
      let result = await AdminUser.sendSenior(req.body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  sendPregnant: async (req, res) => {
    try {
     
      // return;
      let result = await AdminUser.sendPregnant(req.body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  sendPwd: async (req, res) => {
    try {
     
      // return;
      let result = await AdminUser.sendPwd(req.body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  sendFourps: async (req, res) => {
    try {
     
      // return;
      let result = await AdminUser.sendFourps(req.body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  
};
