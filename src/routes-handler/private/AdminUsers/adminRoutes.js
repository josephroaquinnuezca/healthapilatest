const router = require('express').Router();
const AdminUserController = require('./adminController');
const RouteConstant = require('../../../constant/Routes');
const Middleware = require("../../../cors/middleware").checkToken;

module.exports = (app) => {


    router.route('/admin-user-get-all-data')
    .get(AdminUserController.getAllAdminUser);

     
    router.route('/admin-user-get-single-data')
    .get(AdminUserController.getSingleUsersdata);

     router.route('/admin-user-delete-single-data')
    .get(AdminUserController.deleteSingleUsersdata);

    router.route('/admin-user-create-data')
    .post(AdminUserController.CreateAdminUser);

    router.route('/admin-update-single-data')
    .patch(AdminUserController.UpdateAdminUser);


    router.route('/save-medication-info')
    .post(AdminUserController.SaveMedicationInfo);


    router.route('/send-bulk-malnourished')
    .post(AdminUserController.sendMalnourished);

    router.route('/send-bulk-senior')
    .post(AdminUserController.sendSenior);

    router.route('/send-bulk-pregnant')
    .post(AdminUserController.sendPregnant);

    router.route('/send-bulk-pwd')
    .post(AdminUserController.sendPwd);


    router.route('/send-bulk-fourps')
    .post(AdminUserController.sendFourps);



    
    // router.route('/user-get-single-data')
    // .get(UserInfoController.getSingleUserInfo);

    // router.route('/user-update-single-data')
    // .patch(UserInfoController.updateSingleUserInfo);

   

    // router.route('/user-archived-child-status')
    // .patch(UserInfoController.archivedchildstatus);

    // router.route('/user-archived-child-status-history')
    // .get(UserInfoController.getAllChildHistoy);



    app.use(
        RouteConstant.USER_PRIVATE,
        // Middleware,
        router
    );
};
