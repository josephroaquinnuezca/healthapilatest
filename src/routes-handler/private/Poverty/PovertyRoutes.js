const router = require('express').Router();
const PovertyController = require('./PovertyController');
const RouteConstant = require('../../../constant/Routes');
const Middleware = require("../../../cors/middleware").checkToken;

module.exports = (app) => {

    router.route('/poverty-post-data')
    .post(PovertyController.createPoverty);

    router.route('/poverty-get-all-data')
    .get(PovertyController.getAllPoverty);

    router.route('/poverty-get-single-data')
    .get(PovertyController.getSinglePoverty);

    router.route('/poverty-delete-single-data')
    .get(PovertyController.deleteSinglePoverty);

    router.route('/poverty-update-single-data')
    .patch(PovertyController.updateSingleUserPoverty);

    // start
    router.route('/poverty-get-all-data-malnourished')
    .get(PovertyController.getAllMalnourished);

    router.route('/poverty-get-all-data-senior')
    .get(PovertyController.getAllSenior);


    router.route('/poverty-get-all-data-pregnant')
    .get(PovertyController.getAllpregnant);

     //report
     router.route('/poverty-get-all-report')
     .get(PovertyController.getAllPovertyReports)
 
     //get all
     
    router.route('/get-all-pwd')
    .get(PovertyController.getALLPwd);

    router.route('/get-all-fourps')
    .get(PovertyController.getallFourps);
 
    // router.route('/user-update-single-data')
    // .patch(UserInfoController.updateSingleUserInfo);

    // router.route('/user-delete-single-data')
    // .delete(UserInfoController.deleteSingleUserInfo);

    app.use(
        RouteConstant.USER_PRIVATE,
        // Middleware,
        router
    );
};
