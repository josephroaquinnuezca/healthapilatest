const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

const bcrypt = require("bcrypt");

//LOCAL
const PovertyService = require("../../.././services/PovertyService");
// const UtilitiesService = require('../../.././services/UtilitiesService');
const reqResponse = require("../../../cors/responseHandler");
let jwt = require("jsonwebtoken");

const chalk = require("chalk");
const async = require("async");
// const ReusableService = require("../../../reusable_service/reusableService");

module.exports = {
  createPoverty: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        fullname: req_body.fullname,
        cell_no: req_body.cell_no,
        zone: req_body.zone,
        mincome: req_body.mincome,
        occupation: req_body.occupation,
        studying: req_body.studying,
        pwd: req_body.pwd,
        pwdMember: req_body.pwdMember,
        member: req_body.member,
        fourps: req_body.fourps,
        gadgets: req_body.gadgets,
        water: req_body.water,
        house: req_body.house,
      };

      // return;
      let result = await PovertyService.createPoverty(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllPoverty: async (req, res) => {
    try {
      // let id = req.params.id;
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await PovertyService.getAllPoverty(
        limit,
        offset,
        searchString
      );

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getSinglePoverty: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await PovertyService.getSinglePoverty(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  updateSingleUserInfo: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {};

      // return;
      let result = await UserInfoService.updateSingleUserInfo();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  deleteSinglePoverty: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      console.log("Error: delete user", id);

      // return;
      let result = await PovertyService.deleteSinglePoverty(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Deleted.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  updateSingleUserPoverty: async (req, res) => {
    try {

      
      const id = parseInt(req.query.id);
      const req_body = req.body;

      const data = {
        fullname: req_body.fullname,
        mincome: req_body.mincome,
        occupation: req_body.occupation,
        studying: req_body.studying,
        pwd: req_body.pwd,
        member: req_body.member,
        fourps: req_body.fourps,
        gadgets: req_body.gadgets,
        water: req_body.water,
        house: req_body.house,
      };

      // return;
      let result = await PovertyService.updateSingleUserPoverty(data, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  //start

  getAllMalnourished: async (req, res) => {
    try {
      
      // return;
      let result = await PovertyService.getAllMalnourished();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully get all mastered data.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllSenior: async (req, res) => {
    try {
      
      // return;
      let result = await PovertyService.getAllSenior();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully get all mastered data.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  getAllpregnant: async (req, res) => {
    try {
      
      // return;
      let result = await PovertyService.getAllpregnant();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully get all mastered data.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  getAllPovertyReports: async (req, res) => {
    try {

      
      let zones = req.query.zones;

      let result = await PovertyService.getAllPovertyReports(zones);


      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            result
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


  //get all pwd

  getALLPwd: async (req, res) => {
    try {
      
      // return;
      let result = await PovertyService.getALLPwd();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully get all mastered data.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getallFourps: async (req, res) => {
    try {
      
      // return;
      let result = await PovertyService.getallFourps();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully get all mastered data.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end


};
