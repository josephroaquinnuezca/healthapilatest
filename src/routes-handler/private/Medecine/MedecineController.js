const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

const bcrypt = require("bcrypt");

//LOCAL
const MedicineService = require("../../.././services/MedicineService");
// const UtilitiesService = require('../../.././services/UtilitiesService');
const reqResponse = require("../../../cors/responseHandler");
let jwt = require("jsonwebtoken");

const chalk = require("chalk");
const async = require("async");
// const ReusableService = require("../../../reusable_service/reusableService");

module.exports = {
  createMedecine: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        bacthno: req_body.bacthno,
        prodname: req_body.prodname,
        gname: req_body.gname,
        dosage: req_body.dosage,
        medtype: req_body.medtype,
        manufacturer: req_body.manufacturer,
        manufacturingdate: req_body.manufacturingdate,
        exdate: req_body.exdate,
        quantity: req_body.quantity,
        unit: req_body.unit,
      };

      // return;
      let result = await MedicineService.createMedecine(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getAllMedicine: async (req, res) => {
    try {
      // let id = req.params.id;
      let page = parseInt(req.query.page);
      let limit = parseInt(req.query.size);

      let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;
      let result = await MedicineService.getAllMedicine(
        limit,
        offset,
        searchString
      );

      const totalPages = Math.ceil(result.count / limit);

      const res_data = {
        totalCount: result.count,
        totalPages: totalPages,
        pageNumber: page,
        pageSize: result.rows.length,
        data: result.rows,
      };

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get All Data.",
            res_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  getSingleMedicine: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await MedicineService.getSingleMedicine(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  updateSingleMedicine: async (req, res) => {
    try {
      // const { id } = req.params; //:params

      const id = parseInt(req.query.id);

      const req_body = req.body;

      const data = {
        prodname: req_body.prodname,
        gname: req_body.gname,
        dosage: req_body.dosage,
        medtype: req_body.medtype,
        manufacturer: req_body.manufacturer,
        manufacturingdate: req_body.manufacturingdate,
        exdate: req_body.exdate,
        quantity: req_body.quantity,
        unit: req_body.unit,
      };

      // return;
      let result = await MedicineService.updateSingleMedicine(data, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  deleteSingleMedicine: async (req, res) => {
    try {
      const id = parseInt(req.query.id);

      // return;
      let result = await MedicineService.deleteSingleMedicine(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  /*
      getSingleMedication: async (req, res) => {
        try {

       
          const id = parseInt(req.query.id);

          // return;
          let result = await MedicineService.getSingleMedication(id);
    
    
          res
            .status(201)
            .send(
              reqResponse.successResponse(
                201,
                "Successfully login users.",
                result
              )
            );
        } catch (error) {
          console.error("catchFromController: ", error);
          res.status(502).send(reqResponse.errorResponse(502, String(error)));
        }
      }, //end

*/

  //medication  Area
  createMedication: async (req, res) => {
    try {
      const req_body = req.body;

      const data = {
        id: req_body.id,
        med_id: req_body.med_id,
        patient_id: req_body.patient_id,
        quantity: req_body.quantity,
        unit: req_body.quantity,
        med_type: req_body.med_type,
        med_dosage: req_body.ed_dosag,
        created_date: req_body.reated_date,
      };

      // return;
      let result = await MedicineService.createMedication(data);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  //get data in Medication
  getSingleMedication: async (req, res) => {
    try {

      
      const id = parseInt(req.query.id);

      // return;
      let result = await MedicineService.getSingleMedication(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  // get all medicine without pagination
  getAllMeds: async (req, res) => {
    try {

      
      // const id = parseInt(req.query.id);

      // return;
      let result = await MedicineService.getAllMeds();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end

  //remainingstocks

  getRemainingStocks: async (req, res) => {
    try {

      

      // return;
      let result = await MedicineService.getRemainingStocks();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully login users.", result)
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end



};
