const router = require('express').Router();
const MedecineController = require('./MedecineController');
const RouteConstant = require('../../../constant/Routes');
const Middleware = require("../../../cors/middleware").checkToken;

module.exports = (app) => {

    router.route('/medicine-post-data')
    .post(MedecineController.createMedecine);

    
    router.route('/medicine-get-all-data')
    .get(MedecineController.getAllMedicine);
    
    router.route('/medicine-get-single-data')
    .get(MedecineController.getSingleMedicine);

    //medication-data
    router.route('/medication-get-single-data')
    .get(MedecineController.getSingleMedication);
    
    //remainingstocks
    router.route('/medication-remaining-stocks')
    .get(MedecineController.getRemainingStocks);

    router.route('/medicine-update-single-data')
    .patch(MedecineController.updateSingleMedicine);

    router.route('/medicine-delete-single-data')
    .delete(MedecineController.deleteSingleMedicine);

    router.route('/medication-get-single-data')
    .delete(MedecineController.deleteSingleMedicine);


    router.route('/meds-get-all-data')
    .get(MedecineController.getAllMeds);



    // router.route('/user-get-all-data')
    // .get(UserInfoController.getAllUserInfo);

    // router.route('/user-get-single-data')
    // .get(UserInfoController.getSingleUserInfo);

    // router.route('/user-update-single-data')
    // .patch(UserInfoController.updateSingleUserInfo);

    // router.route('/user-delete-single-data')
    // .delete(UserInfoController.deleteSingleUserInfo);

    app.use(
        RouteConstant.USER_PRIVATE,
        // Middleware,
        router
    );
};
