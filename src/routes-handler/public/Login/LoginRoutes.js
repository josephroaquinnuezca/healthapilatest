const router = require("express").Router();
const LoginController = require("./LoginController");
const RouteConstant = require("../../../constant/Routes");
const Middleware = require("../../../cors/middleware").checkToken;

module.exports = (app) => {
  router.route("/login").post(LoginController.authLoginUser);

  router.route("/logout").get(LoginController.authLogoutUser);
  router
    .route("/renew-firebase-token")
    .get(Middleware, LoginController.renewFirebaseToken);

  router.route("/resetpassword").post(LoginController.resetpassword);

  router.route("/resetverify").get(LoginController.resetverify);

  router.route("/reset").post(LoginController.reset);

  router.route("/create-account").post(LoginController.CreateALLClient);

  app.use(
    RouteConstant.USER,
    // Middleware,
    router
  );
};
