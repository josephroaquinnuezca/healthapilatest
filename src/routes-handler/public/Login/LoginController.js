const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

const bcrypt = require("bcrypt");

//LOCAL
const TeamUserSettingService = require("../../../services/TeamUserSettingService");
// const UtilitiesService = require('../../.././services/UtilitiesService');
const reqResponse = require("../../../cors/responseHandler");
let jwt = require("jsonwebtoken");

const chalk = require("chalk");
const async = require("async");
// const ReusableService = require("../../../reusable_service/reusableService");

module.exports = {

  CreateALLClient: async (req, res) => {
    try {
      let user_result = await TeamUserSettingService.CreateALLClient(req.body);

      let dataValues = user_result.dataValues;


      console.log(dataValues);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Create User Account",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }

  },

  authLoginUser: async (req, res) => {
    try {
      console.log("Body", req.body);
      const { username, password } = req.body;
      // console.log(mail_address)
      // console.log(passwd)


         // return;
         let result = await TeamUserSettingService.authLoginUser(
          username,
          password
        );
  
        // add the firebase credentials
        // const retval = await UtilitiesService.firebaseCustomLogin(
        //   result.dataValues.user_id.toString()
        // );
        // result.dataValues.firebase_token = retval;
  
        delete result.dataValues.passwd;
        delete result.dataValues.token;
  
        // create a token
        var token = jwt.sign({ auth_data: result }, corsConfig.secret, {
          expiresIn: 86400, // expires in 24 hours
        });
  
        //PASSWORD ALGORITH HERE
  
        auth_data = {
          // email_address: result.email_address,
          // name: result.name,
          // auth: true,
          data: result.dataValues,
          token: token,
        };
  
        // Set the options for the cookie
        let cookieOptions = {
          // Delete the cookie after 90 days
          expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000),
        };
        if (mode !== "local") {
          // Set the cookie's HttpOnly flag to ensure the cookie is
          // not accessible through JS, making it immune to XSS attacks
          cookieOptions.httpOnly = true;
          // cookieOptions.domain = 'https://healthweb.vercel.app';
  
          // set the cookie's Secure flag
          // to ensure the cookie is only sent over HTTPS
          cookieOptions.secure = true;
        } else {
          cookieOptions.secure = false;
        }
    

      res
        .cookie("jwt", token, cookieOptions)
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully login users.",
            auth_data
          )
        );
    } catch (error) {
      console.error("catchFromController: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  }, //end of login

  authLogoutUser: async (req, res) => {
    try {
      res
        .clearCookie("jwt")
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully gathered user data for logout.",
            "Successfull logout user."
          )
        );
    } catch (error) {
      console.error("catchFromController authLogoutUser: ", error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  renewFirebaseToken: async (req, res) => {
    try {
      // add the firebase credentials
      const retval = await UtilitiesService.firebaseCustomLogin(
        result.dataValues.user_id.toString()
      );

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully renewed firebase token.",
            retval
          )
        );
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  resetpassword: function (req, res) {
    // Input: email
    var mail_address = req.body.mail_address;
    var reset_date = req.body.reset_date;

    console.log("resetpassword", req.body);
    // Process: email URL
    // 1. Check if mail address exists and count of password reset
    // 2. If mail address exists and password reset is less than 6 then
    // 3. Create URL, send email and add password reset count

    // Output:
    // 200 - success request
    // 404 - mail address do not exist
    // 401 - reset count is greater than 5
    // 500 - internal error

    const conn = require("../../../../config/db.js");
    var Reusable = require("./reusable.js.js.js");
    var settings = require("../../../../config/dbsettings.json");

    var sql =
      "SELECT *, user_id FROM user_master WHERE mail_address='" +
      mail_address +
      "' AND status='9'";
    conn.query(sql, function (err, result, fields) {
      if (!err) {
        if (result.length > 0) {
          var user_data = result[0];
          var resetCount = user_data.passwd_reset;
          var resetUrl = "";

          if (Reusable.dateOnly(user_data.password_reset_date) < reset_date) {
            resetCount = 0;
          }

          if (resetCount <= 5) {
            var hashUrl =
              generatePasswordUrl(user_data.email_address, reset_date) + "";
            hashUrl = hashUrl.split("/").join("");
            resetUrl = settings.front_end_url + hashUrl;
            resetCount++;

            // Update database
            var sql = "UPDATE user_master SET";
            sql = sql + makeSqlField(resetUrl, "password_url");
            sql = sql + makeSqlField(reset_date, "password_reset_date");
            sql = sql + " passwd_reset=" + resetCount;
            sql = sql + " WHERE user_id=" + user_data.user_id;
            conn.query(sql, function (err, result, fields) {
              if (!err) {
                // Send email
                var textmsg =
                  "<h4> G-Retail パスワードリセットメール </h4> <br />" +
                  "メール: " +
                  mail_address +
                  "<br />" +
                  "下記のリンクをクリックして、パスワードを再設定してください。<br /><br />" +
                  resetUrl +
                  "<br /> <br />" +
                  "<br /> <br />*このメールはシステムから自動設定されております。";

                const mailjet = require("node-mailjet").connect(
                  "6a8dd8226051b86afdb5ae62b9be304c",
                  "76e544408bd4795d8f51165654b26c1d"
                );
                const request = mailjet
                  .post("send", { version: "v3.1" })
                  .request({
                    Messages: [
                      {
                        From: {
                          Email: "gcrmsp@easycom.co.jp",
                          Name: "G-Retail System",
                        },
                        To: [
                          {
                            Email: mail_address,
                            Name: "G-Retail User",
                          },
                        ],
                        Subject: "G-Retail Reset Password",
                        HTMLPart: textmsg,
                      },
                    ],
                  });
                request
                  .then((result2) => {
                    //console.log("Succeed sending email: " + JSON.stringify(result2));
                    try {
                      var data = {
                        user_id: user_data.user_id,
                        username: user_data.name,
                        route: req.originalUrl,
                        created_at: Reusable.formatedDate(),
                        message: "User request for Reset password",
                        result: "success",
                        req_type: "reset-password",
                      };

                      let bindValue = Object.values(data).fill("?").toString();
                      let key = Object.keys(data).toString();
                      let stmt =
                        "INSERT INTO system_log ( " +
                        key +
                        " ) VALUES ( " +
                        bindValue +
                        " )";
                      let values = Object.values(data);

                      conn.query(stmt, values, function (err, results) {});

                      res.status(200).json({
                        success: true,
                        message: "Successfully acquired password reset URL",
                      });
                    } catch (error) {
                      console.log("then error: " + error.message);
                    }
                  })
                  .catch((err2) => {
                    console.log("Failed sending email: " + err2.statusCode);
                    res.status(400).json({
                      success: false,
                      error: "Bad request. Check logs for errors.",
                    });
                  });
              } else {
                console.log("login: ERROR= " + err);
                res.status(400).json({
                  success: false,
                  error: "Bad request. Check logs for errors.",
                });
              }
            });
          } else {
            console.log("Reset count is greater than 5 today: " + reset_date);
            res.status(401).json({
              success: false,
              error: "reset more than 5 times today",
            });
          }
        } else {
          console.log("no email found= " + mail_address);
          res.status(404).json({
            success: false,
            error:
              "No user with email [" +
              mail_address +
              "] was found in the system.",
          });
        }
      } else {
        console.log("login: ERROR= " + err);
        res.status(400).json({
          success: false,
          error: "Bad request. Check logs for errors.",
        });
      }
    });
  },

  resetverify: function (req, res) {
    var url = req.query.url;

    const conn = require("../../../../config/db.js");

    var sql =
      "SELECT * from user_master where status='9' and password_url='" +
      url +
      "'";
    conn.query(sql, function (err, result, fields) {
      if (!err) {
        if (result.length > 0) {
          user_data = result[0];
          email = user_data.mail_address;

          console.log("return: " + JSON.stringify(user_data));
          console.log("email: " + email);

          res.status(200).json({
            success: true,
            email: email,
            message: "Successfully verified password reset URL",
          });
        } else {
          console.log("Invalid URL: " + url);
          res.status(404).json({
            success: false,
            error: "Invalid URL: " + url,
          });
        }
      } else {
        console.log("login: ERROR= " + err);
        res.status(400).json({
          success: false,
          error: "Bad request. Check logs for errors.",
        });
      }
    });
  },

  reset: function (req, res) {
    // Input: maill, password, URL
    var mail_address = req.body.mail_address;
    var password_url = req.body.password_url;
    var password = req.body.password;

    const conn = require("../../../../config/db.js");
    var Reusable = require("./reusable.js.js.js");
    // Process:
    //   1. Check if mail and URL match
    var sql =
      "SELECT *, user_id FROM user_master WHERE mail_address='" +
      mail_address +
      "' AND password_url='" +
      password_url +
      "'";
    console.log("SQL: " + sql);
    conn.query(sql, function (err, result, fields) {
      if (!err) {
        if (result.length > 0) {
          var oldPassword = checkPasswordValidity(
            result[0].password_old,
            password
          );
          var user_data = result[0];
          console.log("old password: " + oldPassword);

          if (password && oldPassword.length > 0) {
            let hash = bcrypt.hashSync(password, 10);

            //   2. Update the password
            var sql2 = "UPDATE user_master SET";
            sql2 = sql2 + makeSqlField(hash, "passwd");
            sql2 = sql2 + makeSqlField("", "password_url");
            sql2 = sql2 + makeSqlField(oldPassword, "password_old");
            sql2 = sql2 + " passwd_reset=0";
            sql2 =
              sql2 + " WHERE mail_address='" + result[0].mail_address + "'";
            conn.query(sql2, function (err2, result2, fields2) {
              if (!err2) {
                var data = {
                  user_id: user_data.user_id,
                  username: user_data.name,
                  route: req.originalUrl,
                  created_at: Reusable.formatedDate(),
                  message: "User Password is successfully updated",
                  result: "success",
                  req_type: "reset-password",
                };

                let bindValue = Object.values(data).fill("?").toString();
                let key = Object.keys(data).toString();
                let stmt =
                  "INSERT INTO system_log ( " +
                  key +
                  " ) VALUES ( " +
                  bindValue +
                  " )";
                let values = Object.values(data);
                conn.query(stmt, values, function (err, results) {});

                res.status(200).json({
                  success: true,
                  message: "Password is successfully updated",
                });
              } else {
                res.status(401).json({
                  success: false,
                  message: "Failed to update password.",
                });
              }
            });
          } else {
            res.status(401).json({
              success: false,
              error: "Invalid password or password was already used before",
            });
          }
        } else {
          console.log("no email found= " + mail_address);
          res.status(404).json({
            success: false,
            error: "The provided parameter is not existing in the system.",
          });
        }
      } else {
        console.log("login: ERROR= " + err);
        res.status(400).json({
          success: false,
          error: "Bad request. Check logs for errors.",
        });
      }
    });
  },
};

var makeSqlField = function (parameter, fieldname) {
  if (parameter != null) {
    return " " + fieldname + "='" + parameter + "',";
  } else {
    return "";
  }
};

var generatePasswordUrl = function (email, date) {
  return bcrypt.hashSync(email + date, 2);
};

var makeSqlField = function (parameter, fieldname) {
  if (parameter != null) {
    return " " + fieldname + "='" + parameter + "',";
  } else {
    return "";
  }
};

// Check if password was already used before
// blank - already used
// value - new old password
var checkPasswordValidity = function (oldPassword, password) {
  var checkReturn = "";
  if (oldPassword) {
    var checkPass = oldPassword.split("-0-");
    if (checkPass.includes(password)) {
      checkReturn = "";
    } else {
      checkReturn = password;
      for (var i = 0; i < 4; i++) {
        checkReturn = checkReturn + "-0-" + checkPass[i];
      }
    }
  } else {
    checkReturn = password;
  }
  return checkReturn;
};
