mode = process.env.NODE_ENV || 'dev';
const config = require('config').get(mode);
const db = require("./models");
const pack = require('../package');
const express = require('express');


const Index = require('./routes-handler-scheduler/IndexRoutes');
const UtilitiesService = require('./services/UtilitiesService');

const app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// mount routes
require('./routes-handler-scheduler/IndexRoutes')(app);

// catch invalid routes
app.use('*', (req, res) => {
  res.status(403).send('Directory access is forbidden.');
});


// mount server
const server = app.listen(config.port + 1, '0.0.0.0', () => {
  console.log('.......................................');
  console.log(`${config.name} FCM Scheduler`);
  console.log(`Port:\t\t${config.port}`);
  console.log(`Mode:\t\t${config.mode}`);
  console.log(`App version:\t${pack.version}`);
  console.log('.......................................');
});


// reload fcm jobs
UtilitiesService.fcmReloadJobs().then(result => {
  console.log(`${result.length} Scheduled Push Notification Jobs Loaded`);
});


// graceful shutdown
['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach(sig => {
  process.on(sig, () => {
    console.info(`${sig} signal received.`);
    console.log('Closing http server.');

		server.close(err => {
      console.log('Http server closed.');
      if (err) {
        console.error(err);
        process.exit(1);
			}

			process.exit(0);
		})
	})
});

// NODE_ENV=dev pm2 start src/scheduler.js --name g-retail-field-app-scheduler-api --exp-backoff-restart-delay=100 --update-env
// NODE_ENV=staging pm2 start src/scheduler.js --name g-retail-field-app-scheduler-api --exp-backoff-restart-delay=100 --update-env
// NODE_ENV=production pm2 start src/scheduler.js --name g-retail-field-app-scheduler-api --exp-backoff-restart-delay=100 --update-env

// pm2 reload g-retail-field-app-scheduler-api - to restart the api
