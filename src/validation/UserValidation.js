const { body, check } = require('express-validator');

// CONSIST SECTION OF "PRIVATE ROUTES" and "PUBLIC ROUTES"
// SEARCH FOR THE SECTION FOR EASY NAVIGATION.

module.exports = {
  //PRIVATE ROUTES
  create: () => {
    return [
      check("name", "Name is required!").not().isEmpty(),
    ]
  },

  update: () => {
    return [
      check('name', 'Name is Mandatory Parameter Missing.').not().isEmpty()
    ]
  },
  //PUBLIC ROUTES
  login: () => {
    return [
      check('email', 'email is Mandatory Parameter Missing.').not().isEmpty(),
      check('password', 'password is Mandatory Parameter Missing.').not().isEmpty(),
    ]
  }
}
