const joi = require('joi');
const moment = require('moment');
const config = require("config").get(mode);
const keysConfig = config.keys;

//SERVICE
const UtilitiesService = require("../services/UtilitiesService");

//RESPONSE
const reqResponse = require("../cors/responseHandler");

///OTHERS
const async = require("async");

module.exports = {
  fcmScheduledNotify: async (req, res) => {
    try {
      const body_params = {
        schema: {
          title: joi.string().required(),
          body: joi.string().required(),
          destination: joi.array().items(joi.number().min(1)).required(),
          title_body_size: joi.object().custom((val, err) => (Buffer.byteLength(JSON.stringify(val), 'utf8') < 4000) ? val : err.message('combination of title and body must be <= 4000 bytes')),
          schedule: joi.object().keys({
            source: joi.string().valid('memo', 'schedule_add', 'schedule_edit', 'schedule_cancel', 'schedule_duplicate', 'schedule_report', 'schedule_blank', 'schedule_unreported').required(),
            id: joi.number().min(1).required(),
            date_time: joi.string()
              .custom((val, err) => moment(new Date(val)).isAfter(moment()) ? val : err.message('schedule.date_is invalid. Must be after the current date and time'))
              .required(),
          }),
          user_id: joi.number()
        },
        keys: {
          title: req.body.title,
          body: req.body.body,
          title_body_size: {
            notification: {
              title: req.body.title,
              body: req.body.body,
            }
          },
          destination: req.body.destination,
          schedule: req.body.schedule,
          user_id: req.body.user_id,
        }
      };

      const body_res = joi.object().keys(body_params.schema).validate(body_params.keys);
      if (body_res.hasOwnProperty('error')) {
        throw { value: body_res.error.details[0].message };
      }

      const result = await UtilitiesService.fcmScheduledNotify(req.body);

      res.status(201).send(reqResponse.successResponse(201, "Successfully scheduled notification", result));
    } catch(error) {
      res.status(502).send(reqResponse.errorResponse(502, String(error.value)));
    }
  },
  fcmGetScheduled: async (req, res) => {
    try {
      const result = await UtilitiesService.fcmGetScheduled(req.body);

      res.status(201).send(reqResponse.successResponse(201, "Successfully retrieved scheduled notification", result));
    } catch(error) {
      res.status(502).send(reqResponse.errorResponse(502, String(error.value)));
    }
  },
  fcmCancelScheduled: async (req, res) => {
    try {
      const result = await UtilitiesService.fcmCancelScheduled(req.params.firestore_id);

      res.status(201).send(reqResponse.successResponse(201, "Successfully cancelled scheduled notification", result));
    } catch(error) {
      res.status(502).send(reqResponse.errorResponse(502, String(error.value)));
    }
  },
  fcmUpateUserSchedules: async (req, res) => {
    try {
      const body_params = {
        schema: {
          user_id: joi.number().required(),
          schedule_reminder: joi.number().required(),
        },
        keys: {
          user_id: req.body.user_id,
          schedule_reminder: req.body.schedule_reminder,
        }
      };

      const body_res = joi.object().keys(body_params.schema).validate(body_params.keys);
      if (body_res.hasOwnProperty('error')) {
        throw { value: body_res.error.details[0].message };
      }

      const result = await UtilitiesService.fcmUpateUserSchedules(body_res.value);

      res.status(201).send(reqResponse.successResponse(201, "Successfully updated pending scheduled notifications for user", result));
    } catch(error) {
      res.status(502).send(reqResponse.errorResponse(502, String(error.value)));
    }
  },
};
