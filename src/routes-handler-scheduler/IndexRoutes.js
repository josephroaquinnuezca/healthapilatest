const router = require("express").Router();

// CONTROLLER
const IndexController = require("./IndexController");

// DEFAULT ROUTES
const RouteConstant = require("../constant/Routes");

// MIDDLEWARE
const Middleware = require("../cors/middleware").checkToken;

module.exports = (app) => {
  router
    .route("/fcm-notify-user-scheduled")
    .post(IndexController.fcmScheduledNotify); // called by main api. forwards the request here

  router
    .route("/fcm-get-schedule")
    .get(IndexController.fcmGetScheduled);

  router
    .route("/fcm-cancel-schedule/:firestore_id")
    .delete(IndexController.fcmCancelScheduled);

  router
    .route("/fcm-update-user-schedules-alarm")
    .patch(IndexController.fcmUpateUserSchedules); // called by main api. forwards the request here

  app.use(RouteConstant.USER_PRIVATE, Middleware, router);
};
