module.exports = (sequelize, Sequelize) => {
    const MedicineModel = sequelize.define("med_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      bacthno: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      prodname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      gname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      dosage: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      medtype: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      manufacturer: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      manufacturingdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },


      exdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      quantity: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      
      unit: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return MedicineModel;
  };
