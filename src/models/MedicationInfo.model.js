module.exports = (sequelize, Sequelize) => {
    const MedicationModel = sequelize.define("medication_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },

      med_id: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },


      patient_id: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      quantity: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
     
      prev_med_available: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      med_type: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      med_dosage: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      med_manufacturing: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      med_exdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

      status: {
        defaultValue: 9,
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return MedicationModel;
  };
