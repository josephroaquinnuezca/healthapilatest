module.exports = (sequelize, Sequelize) => {
    const TeamUserModel = sequelize.define("user_master", {
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      mail_address: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      passwd: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      role: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.INTEGER
      },
      type: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      name: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      company: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      company_tel: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      cell_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      icon: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      g_acount: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      expire: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      passwd_reset: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      log_count: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      notes: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      password_url: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      // token: {
      //   defaultValue: null,
      //   allowNull: true,
      //   type: Sequelize.STRING
      // },
      // fcm_token: {
      //   defaultValue: null,
      //   allowNull: true,
      //   type: Sequelize.STRING(255)
      // },
      // schedule_reminder: {
      //   defaultValue: null,
      //   allowNull: true,
      //   type: Sequelize.INTEGER(15)
      // },
      status: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.INTEGER(11),
        defaultValue: 9
      },
      accnt_expire: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      password_old: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      password_reset_date: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING,
        onUpdate: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      login_attempt_stamp: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.STRING
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return TeamUserModel;
  };
