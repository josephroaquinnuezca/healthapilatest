module.exports = (sequelize, Sequelize) => {
    const ArchivedDeceasedModel = sequelize.define("archived_deceased", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      patient_id: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      fname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      mname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      lname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      suffix: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      bdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      age: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      sex: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
  
      status: {
        defaultValue: 9,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return ArchivedDeceasedModel;
  };
