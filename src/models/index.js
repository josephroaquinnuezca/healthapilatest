const config = require('config').get(mode);
const dbConfig = config.database;

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  multipleStatements: true,
  dialect: dbConfig.dialect,
  operatorsAliases: dbConfig.operatorsAliases,
  logging: dbConfig.logging,
  define: {
    timestamps: dbConfig.timestamps,
    // freezeTableName: dbConfig.freezeTableName
  },
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// additonal changes
db.TeamUserModel = require("./TeamUser.model.js")(sequelize, Sequelize);

db.MedicationModel = require("./MedicationInfo.model")(sequelize, Sequelize);

db.UserInfoModel = require("./UserInfo.model.js")(sequelize, Sequelize);
db.PovertyModel = require("./PovertyInfo.model")(sequelize, Sequelize);
db.MedicineModel = require("./MedicineInfo.model")(sequelize, Sequelize);

//archived child status
db.ArchivedChildStatusModel = require("./archived_child_status.model")(sequelize, Sequelize);
db.ArchivedWomenStatusModel = require("./archived_womens_period.model")(sequelize, Sequelize);
//archived patay
db.ArchivedDeceasedModel = require("./archived_deceased.model")(sequelize, Sequelize);


Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

module.exports = db;
