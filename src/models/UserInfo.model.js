module.exports = (sequelize, Sequelize) => {
  const UserInfoModel = sequelize.define(
    "user_info_table",
    {
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      unique_code: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      fname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      mname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      lname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      suffix: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      bdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      age: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      sex: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      house_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      zone: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      province: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      city: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      barangay: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      systolic: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      diastolic: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      medical_history: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      cell_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      pwd: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      child_height: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      child_weight: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      child_body_mass_index: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      child_status: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      type: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },

      maintenance_senior: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      maintenance_pregnant: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      pregnant_status: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      type_maintenance: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      last_period: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },

      estimated_period: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(45),
      },

      status: {
        defaultValue: 9,
        allowNull: true,
        type: Sequelize.STRING(255),
      },

      person_status: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255),
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING,
      },
      created_date: {
        type: "TIMESTAMP",
        allowNull: true,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING,
      },
      updated_date: {
        type: "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        allowNull: false,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING,
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,
    }
  );
  // Notification.removeAttribute('id');
  return UserInfoModel;
};
