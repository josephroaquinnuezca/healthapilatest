module.exports = (sequelize, Sequelize) => {
    const PovertyModel = sequelize.define("poverty_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      fullname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      cell_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      zone: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      mincome: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      occupation: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      studying: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      pwd: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      pwdMember: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      member: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      fourps: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      gadgets: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.JSON
      },
      water: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.JSON
      },
      house: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.JSON
      },
      status: {
        defaultValue: 9,
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return PovertyModel;
  };
