module.exports = (sequelize, Sequelize) => {
    const ArchivedChildStatusModel = sequelize.define("archived_child_status_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      user_id: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      fname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      mname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      lname: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      suffix: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      bdate: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      age: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      sex: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      house_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      zone: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      province: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      city: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      barangay: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      cell_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      child_height: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      child_weight: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      child_body_mass_index: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      status: {
        defaultValue: 9,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      created_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      updated_date: {
        type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),

      },
      deleted_by: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      deleted_date: {
        defaultValue: null,
        type: Sequelize.STRING(255),
        allowNull: true,

      },
    }, {
      freezeTableName: true
    });
    // Notification.removeAttribute('id');
    return ArchivedChildStatusModel;
  };
