const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const UserInfoModel = db.UserInfoModel;
const ArchivedChildStatusModel = db.ArchivedChildStatusModel;
const ArchivedWomenStatusModel = db.ArchivedWomenStatusModel;
const ArchivedDeceasedModel = db.ArchivedDeceasedModel;

const Op = db.Sequelize.Op;

const chalk = require("chalk");
const async = require("async");
const bcrypt = require("bcrypt");

const Vonage = require("@vonage/server-sdk");

const randomstring = require("randomstring");

const vonage = new Vonage({
  apiKey: "9e64e5f4",
  apiSecret: "lloXEk9u2zgstFRa",
});

// const Sequelize = require("sequelize");

// const ReusableService = require('../../src/reusable_service/reusableService');

module.exports = {
  // Get Single
  getSingleUniqueCode: async (unique_code) => {
    return new Promise((resolve, reject) => {
      UserInfoModel.findOne({
        where: {
          unique_code: unique_code,
        },
      })

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //CREATE
  createUserInfo: async (data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      var dateObj = new Date();
      var year = dateObj.getUTCFullYear();

      const randomStr = randomstring.generate(7);

      var unique_code = year + randomStr;

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        unique_code: unique_code,
        fname: data.fname,
        mname: data.mname,
        lname: data.lname,
        suffix: data.suffix,
        bdate: data.bdate,
        age: data.age,
        sex: data.sex,
        house_no: data.house_no,
        zone: data.zone,
        province: data.province,
        city: data.city,
        barangay: data.barangay,
        systolic: data.systolic,
        diastolic: data.diastolic,
        medical_history: data.medical_history,
        cell_no: data.cell_no,
        pwd: data.pwd,
        child_height: data.child_height,
        child_weight: data.child_weight,
        child_body_mass_index: data.child_body_mass_index,
        child_status: data.child_status,
        type: data.type,
        maintenance_senior: data.maintenance_senior,
        maintenance_pregnant: data.maintenance_pregnant,
        pregnant_status: data.pregnant_status,
        type_maintenance: data.type_maintenance,
        last_period: data.last_period,
        estimated_period: data.estimated_period,
        person_status: data.person_status,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      if (data_message.person_status == "child") {
        if (data_message.child_status == "Underweight") {
          const from = "Barangay Ipaglaban mo";
          const to = "63" + data_message.cell_no;
          const text =
            "Hello" +
            " " +
            data_message.fname +
            " " +
            data_message.mname +
            " " +
            data_message.lname +
            ". " +
            "You're" +
            " " +
            data_message.child_status +
            ". " +
            "We Recommended that you need to eat more frequently,choose nutrient-rich foods, and have an exercise.We also recommended that you need to attend in our Feeding Program. Thankyou! " +
            " ";// +
            
          //  "https://healthweb.vercel.app/child-landing-page?" + "unique_code=" + unique_code; 

           vonage.message.sendSms(from, to, text, (err, responseData) => {
            if (err) {
              console.log(err);
            } else {
               if (responseData.messages[0]["status"] === "0") {
                 console.log("Message sent successfully.");
               } else {
                 console.log(
                   `Message failed with error: ${responseData.messages[0]["error-text"]}`
                 );
               }
             }
           });
        }else if (data_message.child_status == "Overweight") {
          const from = "Barangay Ipaglaban mo";
          const to = "63" + data_message.cell_no;
          const text =
          "Hello" +
          " " +
          data_message.fname +
          " " +
          data_message.mname +
          " " +
          data_message.lname +
          ". " +
          "You're" +
          " " +
          data_message.child_status +
          ". " +
          "We Recommended that you need to have a proper diet. Thankyou! " +
          " ";// +
          
        //  "https://healthweb.vercel.app/child-landing-page?" + "unique_code=" + unique_code; 

           vonage.message.sendSms(from, to, text, (err, responseData) => {
            if (err) {
              console.log(err);
            } else {
             if (responseData.messages[0]["status"] === "0") {
                 console.log("Message sent successfully.");
               } else {
                 console.log(
                  `Message failed with error: ${responseData.messages[0]["error-text"]}`
                 );
              }
            }
           });
        } else if (data_message.child_status == "Overweight") {
          const from = "Barangay Ipaglaban mo";
          const to = "63" + data_message.cell_no;
          const text =
            "Hello" +
            " " +
            data_message.fname +
            " " +
            data_message.mname +
            " " +
            data_message.lname +
            ". " +
            "You're" +
            " " +
            data_message.child_status +
            ". " +
            "We Recommended that you need to have a proper diet. Thankyou! " +
            " " +
            "Stay healthy and continue eating nutrients food. Thankyou! " +
            " ";// +
            
        //    "https://healthweb.vercel.app/child-landing-page?" + "unique_code=" + unique_code; 

           vonage.message.sendSms(from, to, text, (err, responseData) => {
             if (err) {
              console.log(err);
            } else {
               if (responseData.messages[0]["status"] === "0") {
                 console.log("Message sent successfully.");
               } else {
                console.log(
                   `Message failed with error: ${responseData.messages[0]["error-text"]}`
                 );
               }
             }
           });
        } else if (data_message.child_status == "Normal") {
          const from = "Barangay Ipaglaban mo";
          const to = "63" + data_message.cell_no;
          const text =
            "Hello" +
            " " +
            data_message.fname +
            " " +
            data_message.mname +
            " " +
            data_message.lname +
            ". " +
            "You're" +
            " " +
            data_message.child_status +
            " ." +
            "Stay healthy and continue eating nutrients food. Thankyou! " +
            " " ;//+
          //  "https://healthweb.vercel.app/child-landing-page?" +
          //  "unique_code=" +
          //  unique_code;

           vonage.message.sendSms(from, to, text, (err, responseData) => {
             if (err) {
             console.log(err);
             } else {
               if (responseData.messages[0]["status"] === "0") {
                 console.log("Message sent successfully.");
               } else {
                 console.log(
                   `Message failed with error: ${responseData.messages[0]["error-text"]}`
                 );
               }
             }
           });
        }
      }

      //pregnant

      if (data_message.person_status == "pregnant") {
        const from = "Barangay Ipaglaban mo";
        const to = "63" + data_message.cell_no;
        const text =
          "Hi!" +
          " " +
          data_message.fname +
          " " +
          data_message.mname +
          " " +
          data_message.lname +
          ". " +
          "You're " +
          " " +
          data_message.person_status +
          " ." +
          "Visit our RHU anytime in Wednesday,Thursday for your checkup. Thankyou! " +
          " ";

        //  "https://healthweb.vercel.app/child-landing-page?" + "unique_code=" + unique_code;

        //  vonage.message.sendSms(from, to, text, (err, responseData) => {
        //    if (err) {
        //      console.log(err);
        //    } else {
        //      if (responseData.messages[0]["status"] === "0") {
        //        console.log("Message sent successfully.");
        //      } else {
        //        console.log(
        //          `Message failed with error: ${responseData.messages[0]["error-text"]}`
        //        );
        //      }
        //    }
        //  });
      }

      //senior

      if (data_message.person_status == "senior") {
        const from = "Barangay Ipaglaban mo";
        const to = "63" + data_message.cell_no;
        const text =
          "Hi!" +
          " " +
          data_message.fname +
          " " +
          data_message.mname +
          " " +
          data_message.lname +
          ". " +
          "You're " +
          " " +
          data_message.person_status +
          " ." +
          "Visit our Barangay for your  maintenance . Thankyou! " +
          " ";// +
        //  "https://healthweb.vercel.app/child-landing-page?" +"unique_code=" + unique_code;

         vonage.message.sendSms(from, to, text, (err, responseData) => {
          if (err) {
             console.log(err);
           } else {
             if (responseData.messages[0]["status"] === "0") {
               console.log("Message sent successfully.");
             } else {
               console.log(
                 `Message failed with error: ${responseData.messages[0]["error-text"]}`
               );
             }
           }
         });
      }

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      UserInfoModel.create(data_message)
        .then(async (data) => {
          console.log(
            chalk.green("Function: Create User", JSON.stringify(data))
          );

          // console.log("Data", data);

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });
    });
  },

  getAllUserInfo: async (limit, offset, searchString) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("USER ID:", id));

      const condition = {
        [Op.or]: [
          {
            lname: {
              [Op.like]: `%${searchString}%`,
            },
          },
        ],
        // [Op.or]: [
        //   {
        //     mail_address: {
        //       [Op.like]: `%${searchString}%`,
        //     },
        //   },
        // ],

        [Op.and]: [
          {
            status: 9,
          },
        ],
      };

      UserInfoModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },


  // Get Single
  getSingleUserInfo: async (id) => {
    return new Promise((resolve, reject) => {
      UserInfoModel.findOne({
        where: {
          user_id: id,
        },
      })

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //delete
  // Delete Single
  deleteSingleUserInfo: async (id) => {
    return new Promise((resolve, reject) => {
      UserInfoModel.update(
        {
          status: 0,
        },
        {
          where: {
            user_id: id,
          },
        }
      )

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //Update
  updateSingleUserInfo: async (data, id) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      var condition = id ? { where: { user_id: id } } : null;

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        fname: data.fname,
        mname: data.mname,
        lname: data.lname,
        suffix: data.suffix,
        bdate: data.bdate,
        age: data.age,
        sex: data.sex,
        house_no: data.house_no,
        zone: data.zone,
        province: data.province,
        city: data.city,
        barangay: data.barangay,
        cell_no: data.cell_no,
        pwd: data.pwd,
        // child_height: data.child_height,
        // child_weight: data.child_weight,
        // child_body_mass_index: data.child_body_mass_index,
        // type: data.type,
        // maintenance_senior: data.maintenance_senior,
        // maintenance_pregnant: data.maintenance_pregnant,
        // type_maintenance: data.type_maintenance,
        // last_period: data.last_period,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      UserInfoModel.update(data_message, condition)
        .then(async (data) => {
          console.log(
            chalk.green("Function: CreateTeamUser", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });
    });
  },

  archivedchildstatus: async (data_insert, data_update, id) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      // const t = await db.Sequelize.transaction();

      var insert_data = {
        user_id: data_insert.user_id,
        fname: data_insert.fname,
        mname: data_insert.mname,
        lname: data_insert.lname,
        suffix: data_insert.suffix,
        bdate: data_insert.bdate,
        age: data_insert.age,
        sex: data_insert.sex,
        house_no: data_insert.house_no,
        zone: data_insert.zone,
        province: data_insert.province,
        city: data_insert.city,
        barangay: data_insert.barangay,
        cell_no: data_insert.cell_no,
        pwd: data_insert.pwd,
        child_height: data_insert.child_height,
        child_weight: data_insert.child_weight,
        child_body_mass_index: data_insert.child_body_mass_index,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var update_data = {
        bdate: data_update.bdate_update,
        age: data_update.age_update,
        child_height: data_update.child_height_update,
        child_weight: data_update.child_weight_update,
        child_body_mass_index: data_update.child_body_mass_index_update,
      };

      console.log(update_data);

      var condition = id ? { where: { user_id: id } } : null;
      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      ArchivedChildStatusModel.create(insert_data)
        .then(async (data) => {
          console.log(
            chalk.green("Function: CreateTeamUser", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });

      UserInfoModel.update(update_data, {
        where: {
          user_id: id,
        },
      })
        .then(async (data) => {
          console.log(chalk.green("Function: update", JSON.stringify(data)));

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });

      //try catch

      // await ArchivedChildStatusModel.create(insert_data, {
      //   transaction: t,
      // });

      // await UserInfoModel.update(update_data, {
      //   where: {
      //     user_id: id,
      //   },
      //   transaction: t,
      // });

      // await t
      //   .commit()
    });
  },

  getAllChildHistoy: async (limit, offset, searchString, id) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("USER ID:", id));

      const condition = searchString
        ? {
            [Op.or]: [
              {
                fname: {
                  [Op.like]: `%${searchString}%`,
                },
              },
            ],

            [Op.and]: [
              {
                user_id: id,
              },
            ],

            // [Op.and]: [
            //   {
            //     client_id: id,
            //   },
            // ],
          }
        : null;

      ArchivedChildStatusModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //women
  getAllWomensPeriod: async (limit, offset, searchString, id) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("USER ID:", id));

      const condition = searchString
        ? {
            [Op.or]: [
              {
                date: {
                  [Op.like]: `%${searchString}%`,
                },
              },
            ],

            [Op.and]: [
              {
                user_id: id,
              },
            ],

            // [Op.and]: [
            //   {
            //     client_id: id,
            //   },
            // ],
          }
        : null;

      ArchivedWomenStatusModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //Get all count child status
  getAllChildCount: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as count_normal from user_info_table WHERE child_status = "Normal" ) as count_normal, (SELECT count(*) as count_normal from user_info_table WHERE child_status = "Underweight" ) as count_under, (SELECT count(*) as count_normal from user_info_table WHERE child_status = "Overweight" ) as count_over, (SELECT count(*) as count_child from user_info_table WHERE age <=12 ) as count_child, (SELECT count(*) as count_child_one from user_info_table WHERE zone = "1"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_one , (SELECT count(*) as count_child_two from user_info_table WHERE zone = "2"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_two,(SELECT count(*) as count_child_three from user_info_table WHERE zone = "3"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_three, (SELECT count(*) as count_child_four from user_info_table WHERE zone = "4"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_four, (SELECT count(*) as count_child_five from user_info_table WHERE zone = "5"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_five,(SELECT count(*) as count_child_six from user_info_table WHERE zone = "6"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_six,(SELECT count(*) as count_child_seven from user_info_table WHERE zone = "7"  AND  child_body_mass_index <= 18 AND age <=12) as under_child_seven,(SELECT count(*) as child_one from user_info_table WHERE zone = "1"   AND age <=12) as child_one, (SELECT count(*) as child_two from user_info_table WHERE zone = "2"   AND age <=12) as child_two, (SELECT count(*) as child_three from user_info_table WHERE zone = "3"   AND age <=12) as child_three, (SELECT count(*) as child_four from user_info_table WHERE zone = "4"   AND age <=12) as child_four, (SELECT count(*) as child_five from user_info_table WHERE zone = "5"   AND age <=12) as child_five, (SELECT count(*) as child_six from user_info_table WHERE zone = "6"   AND age <=12) as child_six, (SELECT count(*) as child_seven from user_info_table WHERE zone = "7"   AND age <=12) as child_seven;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //getAllPopulation

  getAllPopulationCount: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as zone_one from user_info_table WHERE zone = "1" AND  age<=150) as zone_one, (SELECT count(*) as zone_two from user_info_table WHERE zone = "2" AND  age<=150) as zone_two, (SELECT count(*) as zone_three from user_info_table WHERE zone = "3" AND  age<=150) as zone_three, (SELECT count(*) as zone_four from user_info_table WHERE zone = "4" AND  age<=150) as zone_four, (SELECT count(*) as zone_five from user_info_table WHERE zone = "5" AND  age<=150) as zone_five, (SELECT count(*) as zone_six from user_info_table WHERE zone = "6" AND  age<=150) as zone_six, (SELECT count(*) as zone_seven from user_info_table WHERE zone = "7" AND  age<=150) as zone_seven, (SELECT count(*) as population from user_info_table WHERE age <=150 ) as population;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //malepopulationzone
  getAllMaleZone: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as zone_one_male from user_info_table WHERE zone="1" AND Sex="Male" ) as zone_one_male, (SELECT count(*) as zone_two_male from user_info_table WHERE zone="2" AND Sex="Male" ) as zone_two_male, (SELECT count(*) as zone_three_male from user_info_table WHERE zone="3" AND Sex="Male" ) as zone_three_male, (SELECT count(*) as zone_four_male from user_info_table WHERE zone="4" AND Sex="Male" ) as zone_four_male, (SELECT count(*) as zone_five_male from user_info_table WHERE zone="5" AND Sex="Male" ) as zone_five_male, (SELECT count(*) as zone_six_male from user_info_table WHERE zone="6" AND Sex="Male" ) as zone_six_male, (SELECT count(*) as zone_seven_male from user_info_table WHERE zone="7" AND Sex="Male" ) as zone_seven_male, (SELECT count(*) as Male from user_info_table WHERE Sex="Male" ) as Male;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //femalepopulationzone
  getAllFemaleZone: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as zone_one_female from user_info_table WHERE zone="1" AND Sex="female" ) as zone_one_gender_female, (SELECT count(*) as zone_two_female from user_info_table WHERE zone="2" AND Sex="Female" ) as zone_two_female, (SELECT count(*) as zone_three_female from user_info_table WHERE zone="3" AND Sex="Female" ) as zone_three_female, (SELECT count(*) as zone_four_female from user_info_table WHERE zone="4" AND Sex="Female" ) as zone_four_female, (SELECT count(*) as zone_five_female from user_info_table WHERE zone="5" AND Sex="Female" ) as zone_five_female, (SELECT count(*) as zone_six_female from user_info_table WHERE zone="6" AND Sex="Female" ) as zone_six_female, (SELECT count(*) as zone_seven_female from user_info_table WHERE zone="7" AND Sex="Female" ) as zone_seven_female, (SELECT count(*) as Total from user_info_table WHERE Sex="Female" ) as Total from user_info_table;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //preggy
  getAllPregnant: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as count_zone_one from user_info_table WHERE zone = "1" AND pregnant_status = "Yes") as count_zone_one,(SELECT count(*) as count_zone_two from user_info_table WHERE zone = "2"  AND  pregnant_status = "Yes") as count_zone_two, (SELECT count(*) as count_zone_three from user_info_table WHERE zone = "3"  AND  pregnant_status = "Yes") as count_zone_three, (SELECT count(*) as count_zone_four from user_info_table WHERE zone = "4"  AND pregnant_status = "Yes") as count_zone_four, (SELECT count(*) as count_zone_five from user_info_table WHERE zone = "5"  AND  pregnant_status = "Yes") as count_zone_five, (SELECT count(*) as count_zone_six from user_info_table WHERE zone = "6"  AND  pregnant_status = "Yes") as count_zone_six, (SELECT count(*) as count_zone_seven from user_info_table WHERE zone = "7"  AND  pregnant_status = "Yes") as count_zone_seven,(SELECT count(*) as pregnant from user_info_table WHERE pregnant_status = "Yes" ) as pregnant;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //senior

  getAllsenior: async () => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          'SELECT distinct (SELECT count(*) as count_senior_one from user_info_table WHERE zone = "1" AND age >=60) as count_senior_one, (SELECT count(*) as count_senior_two from user_info_table WHERE zone = "2"  AND age >=60) as count_senior_two, (SELECT count(*) as count_senior_three from user_info_table WHERE zone = "3"  AND age >=60) as count_senior_three, (SELECT count(*) as count_senior_four from user_info_table WHERE zone = "4"  AND age >=60) as count_senior_four, (SELECT count(*) as count_senior_five from user_info_table WHERE zone = "5"  AND age >=60) as count_senior_five, (SELECT count(*) as count_senior_six from user_info_table WHERE zone = "6"  AND age >=60) as count_senior_six,  (SELECT count(*) as count_senior_seven from user_info_table WHERE zone = "7"  AND age >=60) as count_senior_seven, (SELECT count(*) as senior from user_info_table WHERE age>=60 ) as senior;',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // not yet done
  getAllUsersReport: async (person_status, childStatusSearchString) => {
    return new Promise(async (resolve, reject) => {
      if (person_status == "Child") {
        if (childStatusSearchString == "Normal") {
          await db.sequelize
            .query(
              "SELECT * FROM user_info_table WHERE child_status = :normal",
              {
                logging: console.log,
                replacements: {
                  normal: "Normal",
                },
                plain: false,
                raw: false,
                type: QueryTypes.SELECT,
              }
            )
            .then((data) => {
              console.log(
                chalk.green("getAllUsersReport:", JSON.stringify(data))
              );
              resolve(data);
            })
            .catch((err) => {
              console.log(chalk.red("err: ey", err));
              reject(err);
            });
        } else if (childStatusSearchString == "Underweight") {
          await db.sequelize
            .query(
              'SELECT * FROM user_info_table WHERE child_status = "Underweight"',
              {
                logging: console.log,
                plain: false,
                raw: false,
                type: QueryTypes.SELECT,
              }
            )
            .then((data) => {
              console.log(chalk.green("data:", JSON.stringify(data)));
              resolve(data);
            })
            .catch((err) => {
              console.log(chalk.red("err: ey", err));
              reject(err);
            });
        } else if (childStatusSearchString == "Overweight") {
          await db.sequelize
            .query(
              'SELECT * FROM user_info_table WHERE child_status = "Overweight"',
              {
                logging: console.log,
                plain: false,
                raw: false,
                type: QueryTypes.SELECT,
              }
            )
            .then((data) => {
              console.log(chalk.green("data:", JSON.stringify(data)));
              resolve(data);
            })
            .catch((err) => {
              console.log(chalk.red("err: ey", err));
              reject(err);
            });
        }
      }else if(person_status == "Pregnant"){ //

        await db.sequelize
        .query(
          'SELECT * FROM user_info_table WHERE person_status = "Pregnant"',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });


      }else if(person_status == "Senior"){ //

        await db.sequelize
        .query(
          'SELECT * FROM user_info_table WHERE person_status = "Senior"',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });


      }  //end of iff





    });
  },
  //

  getSingleStatusMalnourished: async () => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: getSingleUsersTeams"));

      await db.sequelize
        .query(
          'SELECT * FROM user_info_table WHERE child_status = "Underweight" ',
          {
            logging: console.log,
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });

      // await TeamMember.findAll({
      //   attributes: ["team_id"],
      //   where: {
      //     member_user_id: {
      //       [Op.eq]: user_id,
      //     },
      //   },
      // })
      //   .then((data) => {
      //     console.log(chalk.green("data:", JSON.stringify(data)));
      //     resolve(data);
      //   })
      //   .catch((error) => {
      //     console.log(chalk.red("error:", error));
      //     var error = "'" + err + "'";
      //     ReusableService.SystemLog(
      //       logInId,
      //       mail_address,
      //       "/get-teams-single-user/:user_id",
      //       error,
      //       "FETCH",
      //       "GET"
      //     );
      //     reject(error);
      //   });
    });
  },

  //CREATE
  addPregnantSchedule: async (data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        user_id: data.user_id,
        womens_date: data.womens_date,
        cell_no: data.cell_no,
        comment: data.comment,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      const from = "Barangay Ipaglaban mo";
      const to = "63" + data_message.cell_no;
      const text =
        "Hello youre following check up will be " + " " + data.womens_date;

     vonage.message.sendSms(from, to, text, (err, responseData) => {
         if (err) {
           console.log(err);
         } else {
           if (responseData.messages[0]["status"] === "0") {
           console.log("Message sent successfully.");
           } else {
            console.log(
               `Message failed with error: ${responseData.messages[0]["error-text"]}`
             );
           }
         }
       });

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      ArchivedWomenStatusModel.create(data_message)
        .then(async (data) => {
          console.log(
            chalk.green("Function: Create User", JSON.stringify(data))
          );

          // console.log("Data", data);

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });
    });
  },

    //deceased

    
  archivedDeceased: async (data, id) => {
    return new Promise((resolve, reject) => {

      var data_message = {
        user_id:id,
        fname: data.fname,
        mname: data.mname,
        lname: data.lname,
        suffix: data.suffix,
        bdate: data.bdate,
        age: data.age,
        sex: data.sex,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      

      UserInfoModel.update(
        {
          status: 0,
        },
        {
          where: {
            user_id: id,
          },
        }
      )

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          ArchivedDeceasedModel.create(data_message)
          .then(async (data) => {
            console.log(
              chalk.green("Function: Create User", JSON.stringify(data))
            );
            resolve(data);
          })
          .catch(async (err) => {
            console.log(chalk.red("Function: CreateTeamUser", err));
            reject(err);
          });

          // resolve(data);/
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },



































};

/*   if (days !== "") {
        // var query_months = "";
        await db.sequelize
          .query(
            "select * from user_info_table where date(created_date) = :days AND zone = :zones AND person_status = :patient_status",
            {
              logging: console.log,
              replacements: {
                days: days_string,
                zones: zones_string,
                patient_status: person_string,
              },
              plain: false,
              raw: false,
              type: QueryTypes.SELECT,
            }
          )
          .then((data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          })
          .catch((err) => {
            console.log(chalk.red("err: ey", err));
            reject(err);
          });
      } else if (months !== "") {
        var query_months =
          "select * from user_info_table where MONTH(created_date) = :months  and zone = :zones AND person_status = :patient_status ";
        await db.sequelize
          .query(query_months, {
            logging: console.log,
            replacements: {
              months: months_string,
              zones: zones_string,
              patient_status: person_string,
            },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          })
          .then((data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          })
          .catch((err) => {
            console.log(chalk.red("err: ey", err));
            reject(err);
          });
      } else if (years !== "") {
        var query_months =
          "select * from user_info_table where YEAR(created_date) = :years  and zone = :zones  AND person_status = :patient_status";
        await db.sequelize
          .query(query_months, {
            logging: console.log,
            replacements: {
              years: years_string,
              zones: zones_string,
              patient_status: person_string,
            },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          })
          .then((data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          })
          .catch((err) => {
            console.log(chalk.red("err: ey", err));
            reject(err);
          });
      } else {
        reject("No result found ");
      }

      */
