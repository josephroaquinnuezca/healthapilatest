const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const MedicineModel = db.MedicineModel;
const MedicationModel = db.MedicationModel;

const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");
const bcrypt = require("bcrypt");

// const ReusableService = require('../../src/reusable_service/reusableService');

module.exports = {
  //CREATE
  createMedecine: async (data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        bacthno: data.bacthno,
        prodname: data.prodname,
        gname: data.gname,
        dosage: data.dosage,
        medtype: data.medtype,
        manufacturer: data.manufacturer,
        manufacturingdate: data.manufacturingdate,
        exdate: data.exdate,
        quantity: data.quantity,
        unit: data.unit,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      MedicineModel.create(data_message)
        .then(async (data) => {
          console.log(
            chalk.green("Function: createPoverty", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: createPoverty", err));
          reject(err);
        });
    });
  },

  //
  getAllMedicine: async (limit, offset, searchString) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("USER ID:", id));

      const condition = searchString
        ? {
            [Op.or]: [
              {
                gname: {
                  [Op.like]: `%${searchString}%`,
                },
              },
            ],

            // [Op.and]: [
            //   {
            //     client_id: id,
            //   },
            // ],
          }
        : null;

      MedicineModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  getSingleMedicine: async (id) => {
    return new Promise((resolve, reject) => {
      MedicineModel.findOne({
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  updateSingleMedicine: async (data, id) => {
    return new Promise(async (resolve, reject) => {
      await MedicineModel.update(data, {
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  deleteSingleMedicine: async (id) => {
    return new Promise((resolve, reject) => {
      MedicineModel.destroy({
        where: {
          id: id,
        },
      })

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //medication
  getSingleMedication: async (id) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
      .query(
        "SELECT DISTINCT	 * FROM med_table as A LEFT JOIN medication_table AS B ON A.id = B.med_id LEFT JOIN user_info_table as C ON B.patient_id = C.user_id where B.patient_id = :id",
        {
          logging: console.log,
          replacements: {
            id: id,
          },
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
        }
      )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // get all medicine withut pagination
  getAllMeds: async () => {
    return new Promise(async (resolve, reject) => {
      MedicineModel.findAll()
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //remainingstocks

  getRemainingStocks: async (id) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
      .query(
        "SELECT DISTINCT gname, quantity, exdate,  datediff(exdate, now() ) as days_remain   FROM med_table WHERE  datediff(exdate, now() ) >=0;",
        {
          logging: console.log,
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
        }
      )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  
};


