const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const TeamUserSetting = db.TeamUserModel;
const TeamMember = db.TeamMember;
const MedicationModel = db.MedicationModel;
const MedicineModel = db.MedicineModel;

const PovertyModel = db.PovertyModel;

const UserInfoModel = db.UserInfoModel;

const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");
const bcrypt = require("bcrypt");

const ReusableService = require("../../src/reusable_service/reusableService");

const Vonage = require("@vonage/server-sdk");
const { object } = require("joi");
const vonage = new Vonage({
  apiKey: "9e64e5f4",
  apiSecret: "lloXEk9u2zgstFRa",
});

module.exports = {
  authLoginUser: async (mail_address, passwd) => {
    console.log(chalk.yellow("Function: authLoginUser"));

    return new Promise(async (resolve, reject) => {
      const condition = mail_address
        ? { where: { mail_address: mail_address } }
        : null;

      TeamUserSetting.findOne(condition)
        .then(async (data) => {
          // console.log(chalk.green('data:', JSON.stringify(data)));
          console.log(chalk.green("data:", data));
          // console.dir(data, { depth: null, colors:true });

          if (data) {
            const ValidatePassword = await bcrypt.compare(passwd, data.passwd);
            if (ValidatePassword) {
              resolve(data);
            } else {
              reject("Password does not match the record we found.");
            }
          } else {
            reject(
              "No result found for login data with username of " + mail_address
            );
          }
        })
        .catch((err) => {
          //  / console.log(chalk.red("err:", err));
          // var error =  "'" + err + "'";
          // ReusableService.SystemLog('1', mail_address, '/login', error, 'POST', 'POST')
          reject(err);
        });
    });
  },

  //CREATE USER FUNCTIONS
  CreateAdminUser: async (UserData) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      var salt = await bcrypt.genSalt(10);
      var password_modify = await bcrypt.hash(UserData.password, salt);

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        mail_address: UserData.mail_address,
        passwd: password_modify,
        role: UserData.user_role,
        type: null,
        name: UserData.name,
        company: null,
        company_tel: null,
        cell_no: UserData.cell_no,
        icon: null,
        g_acount: null,
        expire: null,
        passwd_reset: null,
        log_count: null,
        notes: null,
        password_url: null,
        token: null,
        status: 9,
        accnt_expire: null,
        password_old: null,
        password_reset_date: null,
        login_attempt_stamp: null,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      TeamUserSetting.create(data_message)
        .then(async (data) => {
          console.log(
            chalk.green("Function: CreateTeamUser", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          // ReusableService.SystemLog(logInId, mail_address, '/create-team-user', error, 'CREATE', 'POST')

          reject(err);
        });
    });
  },

  //END OF TESTING

  getAllAdminUser: async (limit, offset, searchString) => {
    return new Promise(async (resolve, reject) => {
      const condition = {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${searchString}%`,
            },
          },
        ],
        // [Op.or]: [
        //   {
        //     mail_address: {
        //       [Op.like]: `%${searchString}%`,
        //     },
        //   },
        // ],

        [Op.and]: [
          {
            status: 9,
          },
        ],
      };
      TeamUserSetting.findAndCountAll({
        order: [["created_date", "DESC"]],
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then(async (data) => {
          console.log(
            chalk.green("Function: getAllTeamUsersData", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: getAllTeamUsersData", err));

          reject(err);
        });
    });
  },

  //GET SINGLE ID

  getSingleUsersdata: async (user_id) => {
    return new Promise(async (resolve, reject) => {
      // var condition = user_id ? { where: { user_id: user_id } } : null;

      TeamUserSetting.findOne({
        where: {
          user_id: user_id,
        },
      })
        .then(async (data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          resolve(data);
        })
        .catch(async (err) => {
          reject(err);
        });
    });
  },

  //DELETE SINGLE ID

  deleteSingleUsersdata: async (user_id) => {
    return new Promise(async (resolve, reject) => {
      var condition = user_id ? { where: { user_id: user_id } } : null;

      const deleted_date = new Date()
        .toISOString()
        .replace(/T/, " ")
        .replace(/\..+/, "");

      TeamUserSetting.update(
        { status: 0, deleted_date: deleted_date },
        condition
      )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  UpdateAdminUser: async (UserDataUpdateData, id) => {
    return new Promise(async (resolve, reject) => {
      var condition = id ? { where: { user_id: id } } : null;

      // console.log("null yung password", UserDataUpdateData.passwd);

      if (UserDataUpdateData.passwd == "") {
        var update_data_message = {
          mail_address: UserDataUpdateData.mail_address,
          // passwd: password_modify,
          role: UserDataUpdateData.user_role,
          type: null,
          name: UserDataUpdateData.name,
          company: nul,
          company_tel: null,
          cell_no: null,
          icon: null,
          g_acount: null,
          expire: null,
          passwd_reset: null,
          log_count: null,
          notes: null,
          password_url: null,
          token: null,
          // status: null,
          accnt_expire: null,
          password_old: null,
          password_reset_date: null,
          login_attempt_stamp: null,
          created_by: null,
          // created_date: null,
          updated_by: null,
          // updated_date: null,
          deleted_by: null,
          // deleted_date: null,
        };

        console.log(
          chalk.red(
            "TEAM UPDATE USER INFO",
            JSON.stringify(update_data_message)
          )
        );

        TeamUserSetting.update(update_data_message, condition, {
          omitNull: false,
        })
          .then(async (data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          })
          .catch(async (err) => {
            console.log(chalk.red("err: ey", err));

            reject(err);
          });
      } else {
        var salt = await bcrypt.genSalt(10);
        var password_modify = await bcrypt.hash(
          UserDataUpdateData.password,
          salt
        );

        //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
        var update_data_message = {
          mail_address: UserDataUpdateData.mail_address,
          passwd: password_modify,
          role: UserDataUpdateData.user_role,
          type: null,
          name: UserDataUpdateData.name,
          company: nul,
          company_tel: null,
          cell_no: null,
          icon: null,
          g_acount: null,
          expire: null,
          passwd_reset: null,
          log_count: null,
          notes: null,
          password_url: null,
          token: null,
          // status: null,
          accnt_expire: null,
          password_old: null,
          password_reset_date: null,
          login_attempt_stamp: null,
          created_by: null,
          // created_date: null,
          updated_by: null,
          // updated_date: null,
          deleted_by: null,
          // deleted_date: null,
        };

        console.log(
          chalk.red(
            "TEAM UPDATE USER INFO",
            JSON.stringify(update_data_message)
          )
        );

        TeamUserSetting.update(update_data_message, condition, {
          omitNull: false,
        })
          .then(async (data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          })
          .catch(async (err) => {
            console.log(chalk.red("err: ey", err));

            reject(err);
          });
      } //if else
    });
  },

  getSingleUsersLeaderStatus: async (user_id, logInId, mail_address) => {
    return new Promise((resolve, reject) => {
      db.sequelize
        .query(
          "SELECT TL.team_id FROM team_leader_setting TL LEFT JOIN team_member_setting TM ON TL.leader_user_id = TM.member_user_id  LEFT JOIN user_master UM ON TM.member_user_id = UM.user_id  WHERE UM.user_id = :id GROUP BY TL.team_id",
          {
            logging: console.log,
            replacements: { id: user_id },
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then(async (data) => {
          resolve(data);
        })
        .catch(async (err) => {
          var error = "'" + err + "'";
          ReusableService.SystemLog(
            logInId,
            mail_address,
            "/get-team-single-leader-status/:user_id",
            error,
            "FETCH",
            "GET"
          );
          reject(err);
        });
    });
  },

  getSingleUsersTeams: async (user_id, logInId, mail_address) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: getSingleUsersTeams"));

      await TeamMember.findAll({
        attributes: ["team_id"],
        where: {
          member_user_id: {
            [Op.eq]: user_id,
          },
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((error) => {
          console.log(chalk.red("error:", error));
          var error = "'" + err + "'";
          ReusableService.SystemLog(
            logInId,
            mail_address,
            "/get-teams-single-user/:user_id",
            error,
            "FETCH",
            "GET"
          );
          reject(error);
        });
    });
  },

  getTeamsUsers: async (query, logInId, mail_address) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: getTeamsUsers"));

      db.sequelize
        .query(query, { type: QueryTypes.SELECT })
        .then(async (data) => {
          data = data.map((data2) => JSON.parse(data2.teams_users));
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch(async (error) => {
          console.log(chalk.red("error:", error));
          var error = "'" + err + "'";
          ReusableService.SystemLog(
            logInId,
            mail_address,
            "/create-visit-type",
            error,
            "FETCH",
            "GET"
          );
          reject(error);
        });
    });
  },

  //

  SaveMedicationInfo: async (UserData) => {
    return new Promise(async (resolve, reject) => {
      var data_message = {
        med_id: UserData.med_id,
        patient_id: UserData.patient_id,
        quantity: UserData.quantity,
        prev_med_available: UserData.med_available,
        med_type: UserData.med_type,
        med_dosage: UserData.med_dosage,
        med_manufacturing: UserData.med_manufacturing,
        med_exdate: UserData.med_exdate,
      };

      console.log(data_message);

      console.log(chalk.red("TEAM USER INFO", JSON.stringify(data_message)));
      MedicationModel.create(data_message)
        .then(async (data) => {
          MedicineModel.update(
            {
              quantity: data_message.prev_med_available,
            },
            {
              where: {
                id: data_message.med_id,
              },
            }
          )
            .then(async (data) => {
              data = data.map((data2) => JSON.parse(data2.teams_users));
              console.log(chalk.green("data:", JSON.stringify(data)));
              resolve(data);
            })
            .catch(async (err) => {
              console.log(chalk.red("Function: SaveMedicationInfo", err));
              // ReusableService.SystemLog(logInId, mail_address, '/create-team-user', error, 'CREATE', 'POST')

              reject(err);
            });

          resolve(data);
          console.log(
            chalk.green("Function: SaveMedicationInfo", JSON.stringify(data))
          );
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: SaveMedicationInfo", err));
          // ReusableService.SystemLog(logInId, mail_address, '/create-team-user', error, 'CREATE', 'POST')

          reject(err);
        });
    });
  },

  // send bulk of malnourished

  sendMalnourished: async (req_data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: sendMalnourished"));

      UserInfoModel.findAll({
        where: {
          child_status: "Underweight",
        },
      })
        .then((data) => {
          const array_cell = [];

          for (item of data) {
            console.log(item["cell_no"]);

            array_cell.push("63" + item["cell_no"]);
          }

          console.log(array_cell);

          const from = "Barangay Ipaglaban mo";

          // const text = comment;

          // const number = "63" + array_cell;
          /*
          array_cell.forEach((number) => {
            vonage.message.sendSms(from, number, text, (err, responseData) => {
              if (err) {
                console.log(err);
              } else {
                if (responseData.messages[0]["status"] === "0") {
                  console.log("Message sent successfully.");
                } else {
                  console.log(
                    `Message failed with error: ${responseData.messages[0]["error-text"]}`
                  );
                }
              }
            });
          }); */
          if (req_data.formattext == 1) {
            const text_format_one =
              "Good Day!  We have a feeding program tomorrow morning.Will be held in our Barangay plaza. Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_one,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
            resolve(data);
          } else if (req_data.formattext == 2) {
            const text_format_two =
              "Good Day!.  We will visit your house tomorrow for the deworming of your children. Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_two,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
            resolve(data);
          } else if (req_data.formattext == 3) {
            const text_format_three =
              "Good Day!.We will get the height and weight of your children it will be held in the barangay plasa.Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_three,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
            resolve(data);
          } else if (req_data.formattext == 4) {
            const text_format_four =
              "Good Day!.You may now get the vitamins of your children and some fruits in our barangay hall.Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_four,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
            resolve(data);
          }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // seniooor

  sendSenior: async (req_data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: sendSenior"));

      UserInfoModel.findAll({
        where: {
          person_status: "senior",
        },
      })
        .then((data) => {
          const array_cell = [];

          for (item of data) {
            console.log(item["cell_no"]);

            array_cell.push("63" + item["cell_no"]);
          }

          console.log(array_cell);

          const from = "Barangay Ipaglaban mo";

          // const text = req_data.formattext;

          // console.log(text);

          if (req_data.formattext == 1) {
            const text_format_one = "Good Day!. Announcement from Brgy. secretary/BHW .Visit our Barangay Hall to get your maintenance.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_one,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
          } else if (req_data.formattext == 2) {
            const text_format_two = "Good Day!Announcement! According to your Senior Citizen President you have a meeting today in the barangay hall. Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_two,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
          } else if (req_data.formattext == 3) {
            const text_format_three = "Good Day!.You have a meeting today in the public auditorium. Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_three,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });
          }

          resolve(data);
          // const number = "63" + array_cell;
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //Pregnant
  sendPregnant: async (req_data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: sendPregnant"));

      UserInfoModel.findAll({
        where: {
          person_status: "pregnant",
        },
      })
        .then((data) => {
          const array_cell = [];

          for (item of data) {
            console.log(item["cell_no"]);

            array_cell.push("63" + item["cell_no"]);
          }

          console.log(array_cell);

          const from = "Barangay Ipaglaban mo";

          //  const text = comment.comment;

          // const numbers = "63" + array_cell;
          /*
          array_cell.forEach((number) => {
            vonage.message.sendSms(from, number, text, (err, responseData) => {
              if (err) {
                console.log(err);
              } else {
                if (responseData.messages[0]["status"] === "0") {
                  console.log("Message sent successfully.");
                } else {
                  console.log(
                    `Message failed with error: ${responseData.messages[0]["error-text"]}`
                  );
                }
              }
            });
          });

          resolve(data); */

          if (req_data.formattext == 1) {
            const text_format_one =
              "Good Day!. Visit our RHU from Wednesday to Thursday for your Chcekup. Thankyou! ";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_one,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          } else if (req_data.formattext == 2) {
            const text_format_two =
              "Announcement. All pregnant women must be inject of an anti tetanu. Visit our RHU .";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_two,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          } else if (req_data.formattext == 3) {
            const text_format_three =
              "Good Day!. We have a symposium for pregnant women. Please go to our public auditorium today.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_three,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //send pwd
  sendPwd: async (req_data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: sendPwd"));

      PovertyModel.findAll({
        where: {
          pwdMember: 1,
        },
      })
        .then((data) => {
          const array_cell = [];

          for (item of data) {
            console.log(item["cell_no"]);

            array_cell.push("63" + item["cell_no"]);
          }

          console.log(array_cell);

          const from = "Barangay Ipaglaban mo";

          //    const text = comment.comment;

          // const numbers = "63" + array_cell;
          /*
          array_cell.forEach((number) => {
            vonage.message.sendSms(from, number, text, (err, responseData) => {
              if (err) {
                console.log(err);
              } else {
                if (responseData.messages[0]["status"] === "0") {
                  console.log("Message sent successfully.");
                } else {
                  console.log(
                    `Message failed with error: ${responseData.messages[0]["error-text"]}`
                  );
                }
              }
            });
          });

          resolve(data); */
          if (req_data.formattext == 1) {
            const text_format_one =
              "Announcement from RHU. Please go to public auditorium to get your fruits and other benefits. Thankyou ";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_one,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          } else if (req_data.formattext == 2) {
            const text_format_two =
              "Good Day!.You may now get the form in the RHU for your ID.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_two,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //4Ps
  sendFourps: async (req_data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("Function: sendFourps"));

      PovertyModel.findAll({
        where: {
          fourps: 1,
        },
      })
        .then((data) => {
          const array_cell = [];

          for (item of data) {
            console.log(item["cell_no"]);

            array_cell.push("63" + item["cell_no"]);
          }

          console.log(array_cell);

          const from = "Barangay Ipaglaban mo";

          // const text = comment.comment;

          // const numbers = "63" + array_cell;
          /*
          array_cell.forEach((number) => {
            vonage.message.sendSms(from, number, text, (err, responseData) => {
              if (err) {
                console.log(err);
              } else {
                if (responseData.messages[0]["status"] === "0") {
                  console.log("Message sent successfully.");
                } else {
                  console.log(
                    `Message failed with error: ${responseData.messages[0]["error-text"]}`
                  );
                }
              }
            });
          });

          resolve(data); */
          if (req_data.formattext == 1) {
            const text_format_one =
              "Announcement from DSWD Parent Leader.We have a meeting in our Barangay Hall.Thankyou.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_one,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          } else if (req_data.formattext == 2) {
            const text_format_two =
              "Good Day!.We have a clean up drive today.Please attend because attendance is a must.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_two,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          } else if (req_data.formattext == 3) {
            const text_format_three =
              "Good Day!.You may now withdraw your ATM for your payout.";
            array_cell.forEach((number) => {
              vonage.message.sendSms(
                from,
                number,
                text_format_three,
                (err, responseData) => {
                  if (err) {
                    console.log(err);
                  } else {
                    if (responseData.messages[0]["status"] === "0") {
                      console.log("Message sent successfully.");
                    } else {
                      console.log(
                        `Message failed with error: ${responseData.messages[0]["error-text"]}`
                      );
                    }
                  }
                }
              );
            });

            resolve(data);
          }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
};
