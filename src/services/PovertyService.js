const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const PovertyModel = db.PovertyModel;
const UserInfoModel = db.UserInfoModel;

const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");
const bcrypt = require("bcrypt");

// const ReusableService = require('../../src/reusable_service/reusableService');

module.exports = {
  //CREATE
  createPoverty: async (data) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        fullname: data.fullname,
        cell_no: data.cell_no,
        zone: data.zone,
        mincome: data.mincome,
        occupation: data.occupation,
        studying: data.studying,
        pwd: data.pwd,
        pwdMember: data.pwdMember,
        member: data.member,
        fourps: data.fourps,
        gadgets: data.gadgets,
        water: data.water,
        house: data.house,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      PovertyModel.create(data_message)
        .then(async (data) => {
          console.log(
            chalk.green("Function: createPoverty", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: createPoverty", err));
          reject(err);
        });
    });
  },

  //
  getAllPoverty: async (limit, offset, searchString) => {
    return new Promise((resolve, reject) => {
      // console.log(chalk.yellow("USER ID:", id));

      // const condition = searchString
      //   ? {
      //     [Op.and]: [
      //       {
      //         fullname: {
      //           [Op.like]: `%${searchString}%`,
      //         },
      //       },
      //     ],
      //     [Op.and]: [
      //       {
      //         status: 9,
      //       },
      //     ],
      //     }
      //   : null;

      PovertyModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: {
          [Op.or]: [
            {
              fullname: {
                [Op.like]: `%${searchString}%`,
              },
            },
          ],
          [Op.and]: [
            {
              status: 9,
            },
          ],
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //
  getSinglePoverty: async (id) => {
    return new Promise((resolve, reject) => {
      PovertyModel.findOne({
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  deleteSinglePoverty: async (id) => {
    return new Promise((resolve, reject) => {
      PovertyModel.update(
        {
          status: 0,
        },
        {
          where: {
            id: id,
          },
        }
      )

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  updateSingleUserPoverty: async (data, id) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("TEAM USER INFO:"));

      var condition = id ? { where: { id: id } } : null;

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        fullname: data.fullname,
        mincome: data.mincome,
        occupation: data.occupation,
        studying: data.studying,
        pwd: data.pwd,
        member: data.member,
        fourps: data.fourps,
        gadgets: data.gadgets,
        water: data.water,
        house: data.house,
        created_by: null,
        // created_date: null,
        updated_by: null,
        // updated_date: null,
        deleted_by: null,
        // deleted_date: null,
      };

      // console.log(chalk.red('TEAM USER INFO' , JSON.stringify(data_message)));
      PovertyModel.update(data_message, condition)
        .then(async (data) => {
          console.log(
            chalk.green("Function: CreateTeamUser", JSON.stringify(data))
          );

          resolve(data);
        })
        .catch(async (err) => {
          console.log(chalk.red("Function: CreateTeamUser", err));
          reject(err);
        });
    });
  },

  // start
  getAllMalnourished: async () => {
    return new Promise((resolve, reject) => {
      UserInfoModel.findAll({
        where: {
          child_status: "Underweight",
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // senior
  getAllSenior: async ()=> {
    return new Promise((resolve, reject) => {
      UserInfoModel.findAll({
        where: {
          person_status: "senior",
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //pwd
  getAllPwd: async () => {
    return new Promise((resolve, reject) => {
      PovertyModel.find({
        where: {
          person_status: "pwd",
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //pregnant

  getAllpregnant: async () => {
    return new Promise((resolve, reject) => {
      UserInfoModel.findAll({
        where: {
          person_status: "pregnant",
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // 

  getAllPovertyReports: async (zones) => {
    return new Promise(async (resolve, reject) => {

      var zones_string = zones == "NaN" ? "" : zones;
      
      await db.sequelize
      .query(
        "SELECT * from poverty_table WHERE zone = :zones",
        {
          logging: console.log,
          replacements: {
            zones: zones_string,
          },
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
        }
      )
      .then((data) => {
        console.log(chalk.green("data:", JSON.stringify(data)));
        resolve(data);
      })
      .catch((err) => {
        console.log(chalk.red("err: ey", err));
        reject(err);
      });
    
    });
  },


  getALLPwd: async () => {
    return new Promise((resolve, reject) => {
      PovertyModel.findAll({
        where: {
          pwdMember: 1,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  getallFourps: async () => {
    return new Promise((resolve, reject) => {
      PovertyModel.findAll({
        where: {
          fourps: 1,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },




};
