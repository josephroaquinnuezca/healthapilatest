{
  "name": "field-app-api",
  "version": "1.0.0",
  "description": "Node.js Rest Apis with Express, Sequelize & MySQL",
  "main": "./src/server.js",
  "private": true,
  "scripts": {
    "dev": "cross-env NODE_ENV=dev FORCE_COLOR=true nodemon --inspect=5001 --config nodemon.js src/server.js",
    "staging": "cross-env NODE_ENV=staging FORCE_COLOR=true nodemon --inspect=5001 --config nodemon.js src/server.js",
    "production": "cross-env NODE_ENV=production FORCE_COLOR=true nodemon --inspect=5001 --config nodemon.js src/server.js"
  },
  "dependencies": {
    "array-refactor": "^1.0.10",
    "async": "3.2.0",
    "body-parser": "1.19.0",
    "chalk": "4.1.0",
    "config": "3.3.3",
    "cors": "2.8.5",
    "ejs": "3.1.5",
    "eslint-config-airbnb": "18.2.1",
    "express": "4.17.1",
    "express-validator": "6.9.2",
    "glob": "7.1.6",
    "jsonwebtoken": "8.5.1",
    "multer": "^1.4.2",
    "mysql2": "^2.2.5",
    "nodemailer": "6.4.17",
    "sequelize": "^6.6.2"
  },
  "devDependencies": {
    "cross-env": "7.0.3",
    "eslint": "7.17.0",
    "eslint-config-airbnb-base": "14.2.1",
    "eslint-config-standard": "16.0.2",
    "eslint-plugin-import": "2.22.1",
    "eslint-plugin-node": "11.1.0",
    "eslint-plugin-promise": "4.2.1",
    "eslint-plugin-standard": "5.0.0",
    "nodemon": "2.0.7"
  },
  "keywords": [
    "field-app-api",
    "nodejs",
    "express",
    "rest",
    "api",
    "sequelize",
    "mysql"
  ],
  "author": "eavsdrop"
}
